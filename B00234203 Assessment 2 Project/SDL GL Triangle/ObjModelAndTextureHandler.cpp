#include "ObjModelAndTexutreHandler.h"
#include <iostream> // included for console output
#include <vector> // for use with the obj loader class

// RT3D CLASS EDITED TO USE MAPS INSTEAD OF VECTORS FOR EASIER/MORE CLEAR ACCESS

// loads in and stores a texture using a passed in texture file name
void ObjModelAndTextureHandler::loadAndStoreTexture(char *textureFileName)
{
	// setting the texture 2d to the textureIDList map, using the filename as the key
	textureIDList[textureFileName] = rt3d::loadBitmap(textureFileName);
}

// takes in a obj model file name and loads in an obj file and then stores the 
// variables required to draw the obj model within a map
void ObjModelAndTextureHandler::loadAndStoreObjModel(char *modelFileName, bool createTangents)
{
	// temporary vectors to help load in the obj model
	std::vector<GLfloat> tempVerts;
	std::vector<GLfloat> tempNorms;
	std::vector<GLfloat> tempTexCoords;
	std::vector<GLuint>  tempIndices;
	std::vector<GLfloat> tempTangents;

	// loads in the obj model
	rt3d::loadObj(modelFileName, tempVerts, tempNorms, tempTexCoords, tempIndices);
	
	// if create tangents is true then
	if(createTangents == true)
		rt3d::createTangents(tempVerts.size(), tempVerts, tempNorms, tempTexCoords, tempIndices, tempTangents);

	// number of indices
	indexCountList[modelFileName] = tempIndices.size();

	// setting the index count and mesh ID onto there respective maps
	objMeshList[modelFileName] = rt3d::createMesh(tempVerts.size()/3, tempVerts.data(), nullptr, tempNorms.data(), tempTexCoords.data(), tempIndices.size(), tempIndices.data());
	
	// if create tangents is true and we have tangent data then
	// add it to the current objects vertex buffer array
	if(createTangents == true && tempTangents.size() > 0)
	{
		glBindVertexArray(objMeshList[modelFileName]);
		GLuint VBO;
		glGenBuffers(1, &VBO);
		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glBufferData(GL_ARRAY_BUFFER, tempTangents.size()*sizeof(GLfloat), tempTangents.data(), GL_STATIC_DRAW);
		glVertexAttribPointer((GLuint)4, 4, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(4);
	}
}

// takes in two GLuints as a reference to a meshID and meshIndexCount to get the ID/Index count and the file name of the obj model
void ObjModelAndTextureHandler::getObjModel(GLuint &meshID, GLuint &meshIndexCount, char *modelFileName)
{
	// finds the obj and index list based on model name
	meshIndexCount = indexCountList.find(modelFileName)->second;
	meshID = objMeshList.find(modelFileName)->second;
}

// takes in a GLuints as a reference to a texture to get the texture ID and an the texture filename to look up
// the correct texture
void ObjModelAndTextureHandler::getTextureID(GLuint &textureID, char *textureFileName)
{
	// finding the texture id from the map using the texture file name 
	textureID = textureIDList.find(textureFileName)->second;
}
// phong-tex.vert
// Vertex shader for use with a Phong or other reflection model fragment shader
// Calculates and passes on V, L, N vectors for use in fragment shader, phong2.frag
#version 400

uniform mat4 modelview;
uniform mat4 projection;
uniform mat4 model;

//uniform mat3 normalmatrix;

in  vec3 in_Position;
in  vec3 in_Normal;

out vec3 ex_N;
out vec3 ex_V;

in vec2 in_TexCoord;
out vec2 ex_TexCoord;
out vec4 mPosition;


// multiply each vertex position by the MVP matrix
// and find V, L, N vectors for the fragment shader
void main(void) {
	// vertex into eye coordinates
	vec4 vertexPosition = modelview * vec4(in_Position,1.0);

	// passing unnormalized and unchanged to -ve eye co-ord vert position
	// will do that there alongside all the other per-fragment light calculations
	ex_V = vertexPosition.xyz;

	// surface normal in eye coordinates
	// taking the rotation part of the modelview matrix to generate the normal matrix
	// (if scaling is includes, should use transpose inverse modelview matrix!)
	// this is somewhat wasteful in compute time and should really be part of the cpu program,
	// giving an additional uniform input
	mat3 normalmatrix = transpose(inverse(mat3(modelview)));
	ex_N = normalize(normalmatrix * in_Normal);
	
	// passing world space position for later conversion and comparing with shadows
	mPosition = model * vec4(in_Position, 1.0);
	
	// passing texture co-ord
	ex_TexCoord = in_TexCoord;

	// clip space co-ordinate
    gl_Position = projection * vertexPosition;

}
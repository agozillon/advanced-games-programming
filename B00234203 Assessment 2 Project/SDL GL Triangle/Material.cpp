#include "Material.h"

Material::Material(vec4 ambient, vec4 diffuse, vec4 specular, float shininess)
{
	material.ambient[0] = ambient.x; material.ambient[1] = ambient.y; material.ambient[2] = ambient.z; material.ambient[3] = ambient.w;
	material.diffuse[0] = diffuse.x; material.diffuse[1] = diffuse.y; material.diffuse[2] = diffuse.z; material.diffuse[3] = diffuse.w;
	material.specular[0] = specular.x; material.specular[1] = specular.y; material.specular[2] = specular.z; material.specular[3] = specular.w;
	material.shininess = shininess;
}


materialStruct Material::getMaterial()
{
	return material;
}

void Material::updateAmbient(vec4 ambient)
{
	material.ambient[0] = ambient.x; material.ambient[1] = ambient.y; material.ambient[2] = ambient.z; material.ambient[3] = ambient.w;
}

void Material::updateDiffuse(vec4 diffuse)
{
	material.diffuse[0] = diffuse.x; material.diffuse[1] = diffuse.y; material.diffuse[2] = diffuse.z; material.diffuse[3] = diffuse.w;
}

void Material::updateSpecular(vec4 specular)
{
	material.specular[0] = specular.x; material.specular[1] = specular.y; material.specular[2] = specular.z; material.specular[3] = specular.w;
}

void Material::updateShininess(float shininess)
{
	material.shininess = shininess;
}
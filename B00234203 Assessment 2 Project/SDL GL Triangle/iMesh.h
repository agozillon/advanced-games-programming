#ifndef IMESH_H
#define IMESH_H
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <GL/glew.h>
#include "Shader.h"

// Simple interface class for renderable meshes, extremely similar to the RT3D
// AbstractMeshHolder class just modified for shadow rendering 
class iMesh{

public:	
	virtual ~iMesh(){}
	// basic constructor takes in files names, shader program, mesh, and the mesh index count
	virtual void draw(glm::mat4 viewMatrix) = 0;               // draws the object with the help of a viewMatrix
	virtual void updateShader(Shader* shader) = 0;             // switch current shader program
	virtual void updateTexture(const GLuint texID) = 0;        // allows changing of the current texture
	virtual void updateShadowCast(const bool castsShadow) = 0; // allows you to mention that the object will have shadows cast on it
	virtual void updatePosition(const glm::vec3 pos) = 0;      // allows you to update the position of the mesh
	virtual void updateRotation(const glm::vec3 rot) = 0;      // allows you to update the rotation of the mesh
	virtual void updateScale(const glm::vec3 scale) = 0;       // allows you to update the scale of the mesh
	virtual glm::vec3 getPosition() = 0;                       // allows you to get the position of the mesh
	virtual glm::vec3 getRotation() = 0;					   // allows you to get the rotation of the mesh
	virtual glm::vec3 getScale() = 0;						   // allows you to get the scale of the mesh
};

#endif
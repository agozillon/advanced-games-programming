// Phong fragment shader phong-tex.frag matched with phong-tex.vert
#version 330

// Some drivers require the following
precision highp float;

vec2 poissonDisk[9] = vec2[](
vec2(-0.4055302f, -0.3266833f),
vec2(0.03130328f, 0.5192217f),
vec2(0.5140005f, -0.5945197f),
vec2(0.3575276f, -0.08809946f),
vec2(-0.5680529f, 0.1590738f),
vec2(-0.05539034f, -0.9256424f),
vec2(0.8778135f, 0.01981898f),
vec2(-0.550568f, 0.715171f),
vec2(0.6164816f, 0.4744883f)
);

struct lightStruct
{
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
	vec4 position;
	vec3 attenuation;
};

struct materialStruct
{
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
	float shininess;
};

uniform lightStruct light;
uniform materialStruct material;
uniform sampler2D textureUnit0;
uniform sampler2DShadow textureUnit1; // ShadowMap
uniform int filterMode;

in vec3 ex_N;
in vec3 ex_V;
in vec2 ex_TexCoord; 
in vec4 shadowCoords;

layout(location = 0) out vec4 out_Color;
 
void main(void) {

    // distance from light to vertex, distance function doesn't work at home
	float distance = abs(length(light.position.xyz - ex_V));
	
	// constant, linear and quadratic
	float atten = 1 / (light.attenuation.x + light.attenuation.y*distance + light.attenuation.z * pow(distance, 2));
	
	vec4 attenVec = vec4(atten, atten, atten, 1);
	
	vec3 ex_L;
	
	// L - to light source from vertex
	if(light.position.w == 1)
	{
		ex_L = normalize(light.position.xyz - ex_V);
	}
	else
	{
		ex_L = normalize(light.position.xyz);
	}

	// Ambient intensity - Ka
	vec4 ambientI = light.ambient * material.ambient;

	// Diffuse intensity - Kd
	vec4 diffuseI = light.diffuse * material.diffuse;
	diffuseI = diffuseI * max(dot(normalize(ex_N),normalize(ex_L)),0);
	
	
	// Specular intensity - Ks
	// Calculate R - reflection of light
	vec3 R = normalize(reflect(normalize(-ex_L),normalize(ex_N)));

	vec4 specularI = light.specular * material.specular;
	specularI = specularI * pow(max(dot(R, normalize(-ex_V)),0), material.shininess);
	float shadowI;
	
	// pretty much how you implement basic shadows using a sampler2DShadow
	// it compares the z co-ordinates of the shadowCoord to 4 texels around
	// the currently being compared texel in the depth map, each compare returns 0 for a fail 
	// and 0.25 for a pass. So we have the possibility to get 0, 0.25, 0.50, 0.75, 1 from the out come.
	if(filterMode == 0) 
	{
		shadowI = textureProj(textureUnit1, shadowCoords); 
	}	
	else if(filterMode == 1) //simple PCF Exert from the red book, not really anyway way to vary this does the same as above except offsetting it diagonally and sampling 16 texels instead of 4 then averaging!
	{
		shadowI = 0;

		shadowI += textureProjOffset(textureUnit1, shadowCoords, ivec2(1, 1));
		shadowI += textureProjOffset(textureUnit1, shadowCoords, ivec2(-1, -1));
		shadowI += textureProjOffset(textureUnit1, shadowCoords, ivec2(-1, 1));
		shadowI += textureProjOffset(textureUnit1, shadowCoords, ivec2(1, -1));
	
		shadowI /= 4;
	}
	else // poisson sampling (neither stratified or rotated right now) help from opengl-tutorial.org, tutorial 16 for this. Uses a poisson disk to sample positions in a circle around the texel, better description in the tutorial 
	{
		shadowI = 1;
	
		for(int i = 0; i < 9; i++){
			shadowI -= 0.1 * (1.0 - texture(textureUnit1, vec3((shadowCoords.xy / shadowCoords.w) + (poissonDisk[i] / 700.0), (shadowCoords.z/shadowCoords.w))));
		}
	}
	
	// Fragment colour 
	out_Color = ambientI + (((diffuseI + specularI) * attenVec) * shadowI) * texture(textureUnit0, ex_TexCoord);
}
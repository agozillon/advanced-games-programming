#ifndef CDEMOSTATE_H
#define CDEMOSTATE_H
#include "iGameState.h"					   // including iGameState so cDemoState can inherit from it 
#include "SkyCubeMap.h"
#include "FirstPersonCamera.h"
#include "ObjModelAndTexutreHandler.h"
#include "ShaderManager.h"
#include <SDL_ttf.h>
#include "ObjMesh.h"
#include "MD2Mesh.h"
#include "Material.h"
#include "Light.h"

using namespace std;

// SIMILAR CONCEPT TO THE GED/RT3D I deleted everything in it to start
// from a blank state template with just Constructor/Deconstructor, void draw/update/init/entry functions

// as you can see below cDemoState is a culmination of all of the inherited Abstract classes functions
class cDemoState: public iGameState{

public:
	cDemoState(){}                               // cDemoState blank inline constructor
	void init();                                       // Function that is used to initilize all of the things that are required to run this state(instead of using the constructor)
    void draw();                                       // Function that is used to draw collections of other objects during this state
    void update();                                     // Function that is used to update this state                                 
	void enter();									   // function used to set certain things upon entry to this state
	~cDemoState();                               // cDemoState destructor

private:
	std::vector<GLfloat> test1;
	std::vector<GLfloat> verts;
	std::vector<GLfloat> normals;
	
	// functions that draw all the objects in the scene to their respective
	// shadow types, shadow map to a normal 2d texture and shadow cube map
	// to a 3D cube map texture
	void shadowCubeMapPass();
	void shadowMapPass();
	
	// values to pass to the shader for shadow map filter
	// methods up and down and turning normal mapping on and off
	int filterValue;
	int normalMapSwitch;

	// cube map skybox
	SkyCubeMap * testCMap;

	// shadow map and depth map related GLuints
	// for holding buffers and texture IDs
	GLuint framebufferID;
	GLuint framebuffer2ID;
	GLuint shadowMapID;
	GLuint depthCube;
	
	// varibles for omni light shadow mapping
	// depth sides just refers to GLenums for cube map sides
	// makes life easier, lightViewMatrices holds the different look
	// at matrices for each side of the point light which we must run 
	// through and render to the depth cube to make it look in all directions
	GLenum depthSides[6];
	glm::mat4 lightViewMatrices[6];

	// TEXTURE STUFF
	GLuint normalTexture;
	GLuint skybox[6];

	// models
	int numberOfObjMesh;
	ObjMesh * objMeshList[6];
	MD2Mesh * md2Model;
	
	// angles for positioning lights
	float angle[8];
	// delay counter for key presses on the light de-activate and activate, ideally would use a timer
	// but this is a quick and easy solution
	int delayCounter;
	// mateirals and lights
	Material * sceneMaterial;
	int numberOfScenePointLights;
	Light * scenePointLights[8];
	Light * sceneShadowMapLight;
	char * lightArrayNames[8];
	bool omniLightActive;
	bool lightRotation;

	// camera, shader manager and model/texture handler classes
	FirstPersonCamera * camera;
	ObjModelAndTextureHandler * modelAndTextureHandler;
	ShaderManager * shaderManager;

	
};

#endif
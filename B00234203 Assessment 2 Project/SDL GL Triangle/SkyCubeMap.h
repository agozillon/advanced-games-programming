#ifndef SKYCUBEMAP_H
#define SKYCUBEMAP_H
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <GL/glew.h>
#include "Shader.h"

// Simple Cubemap class for rendering a skybox, not really refactored for any other use than 
// as a skybox at the moment
class SkyCubeMap{
public:
	// basic constructor takes in files names, shader program, mesh, and the mesh index count
	SkyCubeMap(const char * textureFileName[6], Shader * program, GLuint meshObject, GLuint meshIndexCount);
	~SkyCubeMap();                                 // destructor
	void draw(glm::mat4 viewMatrix);            // draws the cube map from a viewMatrix
	GLuint getSkyCubeMapID(){return SkyCubeMapID;} // returns the first SkyCubeMap texture ID

private:
	GLuint loadSkyCubeMap(const char *fname[6], GLuint * texID); // function for loading in and setting up the SkyCubeMap, takes in 6 strings of chars as texture file names and a GLuint to use as the ID
	Shader * program;        // saves the program ID we use within this SkyCubeMap
	GLuint SkyCubeMapID;   // saves the ID of each texture
	GLuint indexCount;     // index count of the cube
	GLuint meshID;         // ID of the mesh

};

#endif
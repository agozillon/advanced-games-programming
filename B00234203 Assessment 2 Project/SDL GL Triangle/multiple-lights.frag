// Phong fragment shader phong-tex.frag matched with phong-tex.vert
#version 330

// Some drivers require the following
precision highp float;

#define MAXNUMBEROFLIGHTS 8

struct lightStruct
{
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
	vec4 position;
	vec3 attenuation;
	bool lightActive;
};


uniform lightStruct lights[MAXNUMBEROFLIGHTS];

struct materialStruct
{
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
	float shininess;
};


uniform materialStruct material;
uniform sampler2D textureUnit0;

in vec3 ex_N;
in vec3 ex_V;

in vec2 ex_TexCoord; 


layout(location = 0) out vec4 out_Color;

vec4 colorCalc(lightStruct light)
{
	// constant, linear and quadratic
	// distance from light to vertex, distance function doesn't work at home
	float distance = abs(distance(light.position.xyz, ex_V));
	
	// constant, linear and quadratic
	float atten = 1 / (light.attenuation.x + light.attenuation.y*distance + light.attenuation.z * pow(distance, 2));

	// putting into a vec4 so it's easier to multiply against colour values
	vec4 attenVec = vec4(atten, atten, atten, 1);

	// L - to light source from vertex
	vec3 ex_L = normalize(light.position.xyz - ex_V);

	vec4 test = vec4(0, 0, 0, 0);
	// Ambient intensity - Ka
	vec4 ambientI = light.ambient * material.ambient;
	test += ambientI * attenVec;
	 	
	// Diffuse intensity - Kd
	vec4 diffuseI = light.diffuse * material.diffuse;
	diffuseI += diffuseI * max(dot(normalize(ex_N),normalize(ex_L)),0);
	test += diffuseI * attenVec;	
	
	// Specular intensity - Ks
	// Calculate R - reflection of light
	vec3 R = normalize(reflect(normalize(-ex_L),normalize(ex_N)));
	
	vec4 specularI = light.specular * material.specular;
	specularI += specularI * pow(max(dot(R,normalize(-ex_V)),0), material.shininess);
	test+= specularI * attenVec;

	return test;
}

void main(void) {
   		
	vec4 maxColor = vec4(0, 0, 0, 1);

	for(int i = 0; i < MAXNUMBEROFLIGHTS; i++)
	{
		if(lights[i].lightActive == true)
		{
			maxColor += colorCalc(lights[i]);
		}
	}
	
	clamp(maxColor, vec4(0, 0, 0, 0), vec4(1, 1, 1, 1));

	// Fragment colour * attenVec
	out_Color = maxColor * texture(textureUnit0, ex_TexCoord);
	//out_Color = texture2D(textureUnit0, ex_TexCoord);
}
#include "FirstPersonCamera.h"

//USED PREVIOUSLY IN RT3D, but modified slightly for use with this project (tilt up and down/pitch)

#define DEG_TO_RADIAN 0.017453293

// constructor that sets default values up for the FirstPersonCamera
FirstPersonCamera::FirstPersonCamera()
{
	// calling the update functions to make the constructor shorter
	updateEye(vec3(0.0, 1.0, 0.0));
	updateAt(vec3(0.0, 1.0, -1.0));
	updateUp(vec3(0.0, 1.0, 0.0));

	yaw = 0;
	pitch = 0;
}


// moves the camera forward or backwards using a passed in distance
// for the eye and using the eye value and a defualt 1 value to move the at
// so the camera is always looking a set distance ahead
void FirstPersonCamera::moveForwardOrBackwards(float dist)
{
	eye = glm::vec3(eye.x + dist*std::sin(yaw*DEG_TO_RADIAN), eye.y + dist * std::tan(pitch*DEG_TO_RADIAN), eye.z - dist*std::cos(yaw*DEG_TO_RADIAN));
	at = glm::vec3(eye.x + 1.0*std::sin(yaw*DEG_TO_RADIAN), eye.y + 1.0 * std::tan(pitch*DEG_TO_RADIAN), eye.z - 1.0*std::cos(yaw*DEG_TO_RADIAN));
}

// moves the camera right or left using a passed in distance
// for the eye and move right and using the eye value and a defualt 1 
// value with the moveForward function to move the at so the camera is 
// always looking a set distance ahead of the eye
void FirstPersonCamera::moveRightOrLeft(float dist)
{
	eye = glm::vec3(eye.x + dist*std::cos(yaw*DEG_TO_RADIAN), eye.y, eye.z + dist*std::sin(yaw*DEG_TO_RADIAN));
	at =  glm::vec3(eye.x + 1.0*std::sin(yaw*DEG_TO_RADIAN), eye.y + 1.0 * std::tan(pitch*DEG_TO_RADIAN), eye.z - 1.0*std::cos(yaw*DEG_TO_RADIAN));
}

// moves the camera right or left using a passed in distance
// for the eye and move right and using the eye value and a defualt 1 
// value with the moveForward function to move the at so the camera is 
// always looking a set distance ahead of the eye
void FirstPersonCamera::moveUpOrDown(float dist)
{
	eye = glm::vec3(eye.x, eye.y+dist, eye.z);
	at = glm::vec3(eye.x + 1.0*std::sin(yaw*DEG_TO_RADIAN), eye.y + 1.0 * std::tan(pitch*DEG_TO_RADIAN), eye.z - 1.0*std::cos(yaw*DEG_TO_RADIAN));
}

// rotates the camera right or left by rotating the position the camera is staring
// at via the moveForward function and a passed in rotation value
void FirstPersonCamera::updateYaw(float rot)
{
	yaw += rot;
	at = glm::vec3(eye.x + 1.0*std::sin(yaw*DEG_TO_RADIAN), eye.y + 1.0 * std::tan(pitch*DEG_TO_RADIAN), eye.z - 1.0*std::cos(yaw*DEG_TO_RADIAN));
}

// updates the pitch of the camera up or down, locks it to a certain value
// so that it doesn't mess around with move up or down or cause the up vectortochange. 
void FirstPersonCamera::updatePitch(float rot)
{
	pitch += rot;

	if(pitch > 70)
		pitch = 70;
	else if(pitch <= -70)
		pitch = -70;

	at = glm::vec3(eye.x + 1.0*std::sin(yaw*DEG_TO_RADIAN), eye.y + 1.0 * std::tan(pitch*DEG_TO_RADIAN), eye.z - 1.0*std::cos(yaw*DEG_TO_RADIAN));
}
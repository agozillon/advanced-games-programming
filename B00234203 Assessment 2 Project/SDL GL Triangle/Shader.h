#ifndef SHADER_H
#define SHADER_H
#include <GL\glew.h>
#include "rt3d.h" // need this for structs at the moment.
#include "Material.h"
#include "Light.h"

// shader class containts the current shader program stored witin 
// alongside functions to interact and send variables to the shader stored within
class Shader{

public:
	Shader(){}
	~Shader(){}
	GLuint getShaderID(){return shaderID;}                                              // returns current shaderID
	void setShaderID(GLuint newShaderID){shaderID = newShaderID;}						// sets the shaderID
	void setUniformMatrix4fv(const char* uniformName, const GLfloat *data);             // sends a matrix 4x4 to the shader
	void setUniformVec3fv(const char* uniformName, float x, float y, float z);          // sends a vec3 to the shader
	void setUniformFloat(const char * uniformName, const GLfloat data);                 // sends an single float to the shader
	void setLight(const lightStruct light);                                             // sends a light structure to the shader
	void setUniformVec4fv(char * uniformName, const GLfloat *data);                     // sends a vec4 to the shader 
	void setMaterial(const materialStruct material);                                    // sends a material structure to the shader
	void setUniformi(char * uniformName, const GLuint assignedIndex);                   // sends an integer to the shader
	void setMultipleLightPos(glm::vec4 lightPos[],  int numberOf, char * uniformNames[]); // sends multiple light positions to the shader
	void setActiveShader();																  // sets the shader held within this shader object as the current active shader in use by OpenGL
	void setLights(lightStruct light[], int numberOf, char * uniformNames[]);             // sends multiple light structures to the shader

private:
	GLuint shaderID; // holds the ID for the shader
	

};

#endif
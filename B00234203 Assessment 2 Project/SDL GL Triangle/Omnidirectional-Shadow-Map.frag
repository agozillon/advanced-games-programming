// Phong fragment shader phong-tex.frag matched with phong-tex.vert
#version 400

// Some drivers require the following
precision highp float;

#define MAXNUMBEROFLIGHTS 8

struct lightStruct
{
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
	vec4 position;
	vec3 attenuation;
	bool lightActive;
};

struct materialStruct
{
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
	float shininess;
};

uniform lightStruct lights[MAXNUMBEROFLIGHTS];

uniform materialStruct material;
uniform sampler2D textureUnit0;
uniform samplerCube textureUnit1; // ShadowMap
uniform float lightFarPlane; // far plane value of light to multiply values in texture back into comparable values

in vec3 ex_N;
in vec3 ex_V;
in vec2 ex_TexCoord;
in vec4 mPosition;


layout(location = 0) out vec4 out_Color;


vec4 colorCalc(lightStruct light, float shadowIntensity)
{
	// constant, linear and quadratic
	// distance from light to vertex, distance function doesn't work at home
	float distance = distance(light.position.xyz, ex_V);
	
	// calculate attenuation (all lights use the same attenuation at the moment)
	float atten = 1 / (light.attenuation.x + light.attenuation.y*distance + light.attenuation.z * pow(distance, 2));
	
	// vec4 of attenuation values for easier comparison
	vec4 attenVec = vec4(atten, atten, atten, 1);
	
	// L - to light source from vertex
	vec3 ex_L = normalize(light.position.xyz - ex_V);

	vec4 test = vec4(0, 0, 0, 0);
	// Ambient intensity - Ka
	vec4 ambientI = light.ambient * material.ambient;
	test += ambientI;
	 	
	// Diffuse intensity - Kd
	vec4 diffuseI = light.diffuse * material.diffuse;
	diffuseI = diffuseI * max(dot(normalize(ex_N),normalize(ex_L)),0);
	test += diffuseI * shadowIntensity;	
	
	// Specular intensity - Ks
	// Calculate R - reflection of light
	vec3 R = normalize(reflect(normalize(-ex_L),normalize(ex_N)));
	
	vec4 specularI = light.specular * material.specular;
	specularI = specularI * pow(max(dot(R,normalize(-ex_V)),0), material.shininess);
	test += specularI * shadowIntensity;	

	test.rgb = test.rgb * attenVec.rgb;
	test.w = 1.0;

	return test;
}

void main(void) {

	// had a fair amount of help from http://www.cg.tuwien.ac.at/courses/Realtime/repetitorium/2011/OmnidirShadows.pdf
	// to get over the final comparrison challenge as I couldn't quite workout the format in which
	// to test the look up value (distance from light to frag) and the depth value pulled from the texture in!
	vec3 cubeLookUp = mPosition.xyz - lights[0].position.xyz;
	
	// 	calculating the length of the vector, so we can compare it to the depth
	float distToLight = length(cubeLookUp);
	
	/*
	float shadowI = 1.0;

	float depthMapValue = texture(textureUnit1, cubeLookUp).r;

	depthMapValue = depthMapValue * lightFarPlane;

	if(depthMapValue+0.25 < distToLight)
		shadowI = 0.0;
	*/
			
	// TO GET NON-PCF OMNIDIRECTIONAL SHADOW MAP (for GLSL versions lower than 4.0! as
	// textureGather is a 4.0 function) comment out the code starting here (and ending below
	// at another message) you then uncomment the above commented out lines of code and it should
	// work! you'd also want to switch GLSL version to #330 and OpenGL context version in setupRC 
	// on the CPU side
	
	vec3 l = normalize(lights[0].position.xyz - ex_V);
	
	// simple PCF run through gathering 4 neighbouring texels then testing them!
	float shadowI = 1.0;

	vec4 test = textureGather(textureUnit1, cubeLookUp, 0);
	
	// "unpackaging" the distances in the depth map via the light far plane again
	// basically distances from the light to the nearest occluding object (in most cases
	// sometimes it might not be occluded!
	test.r = test.r * lightFarPlane;
	test.g = test.g * lightFarPlane;
	test.b = test.b * lightFarPlane;
	test.a = test.a * lightFarPlane;

	// the actual testing of values test.rgba being the 4 depth values pulled from the
	// function in textureGather alongside a shadow offset bias that's changeable
	// on a per model basis and the distToLight being the current fragments distance 
	//from the light, negate 0.25 from the shadow intensity if the depth map value is closer than the fragment value! 
	if(test.r < distToLight)
	{
		shadowI -= 0.20;
	}

	if(test.g < distToLight)
	{
		shadowI -= 0.20;
	}

	if(test.b  < distToLight)
	{
		shadowI -= 0.20;
	}

	if(test.a < distToLight)
	{
		shadowI -= 0.20;
	}
	
	// and ending here

	vec4 maxColor = vec4(0, 0, 0, 1);

	for(int i = 0; i < MAXNUMBEROFLIGHTS; i++)
	{
		if(lights[i].lightActive == true)
		{
			maxColor += colorCalc(lights[i], shadowI);
		}
	}

	clamp(maxColor, vec4(0, 0, 0, 0), vec4(1, 1, 1, 1));
	
	// Fragment colour
	out_Color = maxColor * texture(textureUnit0, ex_TexCoord);
}
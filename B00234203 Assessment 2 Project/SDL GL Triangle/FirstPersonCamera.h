#ifndef FIRSTPERSONCAMERA_H
#define FIRSTPERSONCAMERA_H
#include "iCamera.h" // including the iCameraClass so the FirstPersonCamera class can inherit from it and has access to glm lookAt and vec3s		
#include <glm/gtc/matrix_transform.hpp> // including matrix_transform for lookAt

// USED PREVIOUSLY IN RT3D

// basic First Person Camera class that inherits from iCamera and can be used to move a camera around a scene
class FirstPersonCamera: public iCamera
{

public:
	FirstPersonCamera();                                       // constructor sets up default values for the first person camera
	~FirstPersonCamera(){}									   // blank inline destructor
	vec3 getEye(){return eye;}								   // inline function that returns the cameras Eye position as a vec3
	vec3 getAt(){return at;}								   // inline function that returns the cameras At position as a vec3
	vec3 getUp(){return up;}								   // inline function that returns the cameras Up position as a vec3
	void updateEye(vec3 pos){eye = pos;}					   // inline function that updates the Eye position via a passed in vec3 
	void updateAt(vec3 pos){at = pos;}						   // inline function that updates the At position via a passed in vec3 
	void updateUp(vec3 pos){up = pos;}		                   // inline function that updates the Up position via a passed in vec3 
	void updateYaw(float rot);								   // function that updates the cameras rotation and updates the camera
	float getYaw(){return yaw;}								   // inline function that returns the cameras yaw as a float
	void updatePitch(float rot);							   // function that updates the cameras pitch and updates the camera
	float getPitch(){return pitch;}                            // inline function that returns the cameras pitch as a float
	mat4 getCurrentCamera(){return lookAt(eye, at, up);}	   // inline function that returns the cameras LookAt as a mat4
	void moveForwardOrBackwards(float dist);				   // function used to move the camera forwards or backwards by a passed in distance
	void moveRightOrLeft(float dist);						   // function used to move the camera left or right by a passed in distance
	void moveUpOrDown(float dist);							   // function used to move the camera up or down by a passed in distance

private:
	
	// variables to hold the cameras Rotation/Eye/At/Up  
	float yaw;
	float pitch;

	vec3 eye;
	vec3 at;
	vec3 up;

};

#endif
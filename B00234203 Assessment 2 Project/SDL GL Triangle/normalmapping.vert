// phong-tex.vert
// Vertex shader for use with a Phong or other reflection model fragment shader
// Calculates and passes on V, L, N vectors for use in fragment shader, phong2.frag
#version 330

struct lightStruct
{
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
	vec4 position;
	vec3 attenuation;
	bool lightActive;
};

uniform mat4 modelview;
uniform mat4 projection;
uniform lightStruct light;
uniform bool switchNormal;
uniform mat4 model;
//uniform mat3 normalmatrix;

in  vec3 in_Position;
in  vec3 in_Normal;
in  vec4 in_Tangent;

out vec3 ex_N; // output normal, this normal is only used if normal mapping is turned off!
out vec3 ex_V; // output vertex
out vec3 ex_L; // output light direction

in vec2 in_TexCoord;
out vec2 ex_TexCoord;

// multiply each vertex position by the MVP matrix
// and find V, L, N vectors for the fragment shader
void main(void) {

	// vertex into eye coordinates
	vec4 vertexPosition = modelview * vec4(in_Position,1.0);
	
	// surface normal in eye coordinates
	// taking the rotation part of the modelview matrix to generate the normal matrix
	// (if scaling is includes, should use transpose inverse modelview matrix!)
	// this is somewhat wasteful in compute time and should really be part of the cpu program,
	// giving an additional uniform input
	mat3 normalmatrix = transpose(inverse(mat3(modelview)));
	ex_N = normalize(normalmatrix * in_Normal);
	
	// if normal mapping is on go through all of the calculations relevant
	// to normal mapping else do the normal light and vertex calculations
	if(switchNormal == true)
	{
		// inverse model matrix used to move the lights position
		// from world space into model space
		mat4 invModel = inverse(model);
		vec4 lightPosObject = invModel * vec4(light.position.xyz, 1); 
	
		// calculating bitangent, cross product of tangent and normal, the W scales it -ve or positive
		// depending on the handidness of the tangent/vertices
		vec3 bitangent = (cross(in_Tangent.xyz, in_Normal) * in_Tangent.w);
		vec3 wNorm = in_Normal;
		vec3 tangent = in_Tangent.xyz;

		// creating TBN matrix that can transfer from Object/Model Space to TBN Space
		mat3 TBN = mat3(tangent.xyz, bitangent, wNorm);
	
		// L - to light source from vertex/lightposition 
	
		// calculates the direction of the light in model space, from the vertex in model space
	    // and then transfers it into tangent space
		ex_L = TBN * (vec4(lightPosObject.xyz, 0.0).xyz - in_Position.xyz).xyz;
	
		// Find V - in eye coordinates, eye is at (0,0,0) 
		ex_V = TBN * in_Position;
	}
	else if(switchNormal == false) // normal light calculations
	{
		// L - to light source from vertex/lightposition
		ex_L = normalize(light.position.xyz - vertexPosition.xyz);

		ex_V = normalize(vertexPosition).xyz;
	}

	ex_TexCoord = in_TexCoord;

    gl_Position = projection * vertexPosition;

}
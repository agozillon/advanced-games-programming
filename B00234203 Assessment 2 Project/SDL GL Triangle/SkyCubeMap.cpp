#include "SkyCubeMap.h"
#include <SDL.h>
#include <iostream>
#include "rt3d.h"
#include <stack>


// constructor calls private loadSkyCubeMap function to setup andload textures in, then assigns passed in variables to class variables
SkyCubeMap::SkyCubeMap(const char * textureFileName[6], Shader * shaderProgram, GLuint meshObject, GLuint meshIndexCount)
{
	program = shaderProgram;

	loadSkyCubeMap(textureFileName, &SkyCubeMapID);

	indexCount = meshIndexCount;
	meshID = meshObject;
}

SkyCubeMap::~SkyCubeMap()
{

}

// private function for loading in and setting up a SkyCubeMap
GLuint SkyCubeMap::loadSkyCubeMap(const char *fname[6], GLuint *texID)
{
	glGenTextures(1, texID); // generate texture ID
	
	glActiveTexture(GL_TEXTURE0); // make sure correct texture location is active

	// GL enumeration holding enum values to all sides of the Cube map 
	// useful for looping through them 
	GLenum sides[6] = { GL_TEXTURE_CUBE_MAP_POSITIVE_Z,
	GL_TEXTURE_CUBE_MAP_NEGATIVE_Z,
	GL_TEXTURE_CUBE_MAP_POSITIVE_X,
	GL_TEXTURE_CUBE_MAP_NEGATIVE_X,
	GL_TEXTURE_CUBE_MAP_POSITIVE_Y,
	GL_TEXTURE_CUBE_MAP_NEGATIVE_Y };

	SDL_Surface *tmpSurface; // tmp SDL surface to load the bmp texture into

	// setting up the normal texture filtering and wrapping params + 
	glBindTexture(GL_TEXTURE_CUBE_MAP, *texID); // bind texture and set parameters
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, 
	GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, 
	GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, 
	GL_CLAMP_TO_EDGE);
	
	// loops through loads in the textures, creates a texutre image 2d, stores the texture and sets
	// parameters and then frees the surfaace
	for (int i=0;i<6;i++)
	{
		// load file - using core SDL library
		tmpSurface = SDL_LoadBMP(fname[i]);
	
		if (!tmpSurface)
		{
			std::cout << "Error loading bitmap" << std::endl;
			return *texID;
		}
		
		glTexImage2D(sides[i],0,GL_RGB,tmpSurface->w, tmpSurface->h, 0,
		GL_BGR, GL_UNSIGNED_BYTE, 
		tmpSurface->pixels);
		// texture loaded, free the temporary buffer
		SDL_FreeSurface(tmpSurface);
	}

	glActiveTexture(0); // changing the active texture to the default

	return *texID; // return value of texure ID, redundant really
}

// draw the cube map, simply drawn the same as the skybox definetly not the 
// best way to render a SkyCubeMap as it'll essentially always draw as a skybox
// maybe a good idea to have 2 seperate draw functions one for skybox and one for
// a regular render.
void SkyCubeMap::draw(glm::mat4 viewMatrix)
{
	// set up camera/eye position 
	// then render skybox as single cube using cube map
	program->setActiveShader();
	
	std::stack<glm::mat4> mvStack;

	glDepthMask(GL_FALSE); // make sure writing to update depth test is off
	glm::mat3 rotationOnly = glm::mat3(viewMatrix); // get the rotation of the view matri

	mvStack.push(glm::mat4(rotationOnly)); // push it

	glCullFace(GL_FRONT); // drawing inside of cube
	glBindTexture(GL_TEXTURE_2D, SkyCubeMapID);
	mvStack.top() = glm::scale(mvStack.top(), glm::vec3(1.5f, 1.5f, 1.5f));
	program->setUniformMatrix4fv("modelview", glm::value_ptr(mvStack.top()));
	rt3d::drawIndexedMesh(meshID, indexCount, GL_TRIANGLES);
	mvStack.pop();
	glCullFace(GL_BACK);

	glDepthMask(GL_TRUE);
}
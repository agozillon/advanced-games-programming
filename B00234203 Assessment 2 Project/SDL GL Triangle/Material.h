#ifndef MATERIAL_H
#define MATERIAL_H
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <GL/glew.h>
using namespace glm;

// material structure removed from rt3d
struct materialStruct {
		GLfloat ambient[4];
		GLfloat diffuse[4];
		GLfloat specular[4];
		GLfloat shininess;
	};

// Simple material class, pretty much the same as RT3D minus a few useless functions
class Material{
public:
	// basic constructor takes the ambient, diffuse, specular and shininess of a material
	Material(vec4 ambient, vec4 diffuse, vec4 specular, float shininess);
	~Material(){}                          // destructor
	materialStruct getMaterial();		   // get material struct
	void updateAmbient(vec4 ambient);      // change the ambient values
	void updateDiffuse(vec4 diffuse);      // change the diffuse values
	void updateSpecular(vec4 specular);    // change the specular values
	void updateShininess(float shininess); // change the shininess values

private:
	materialStruct material; // material
};

#endif
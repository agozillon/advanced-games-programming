#ifndef GAMECONTROL_H
#define GAMECONTROL_H
#include <SDL_ttf.h>   // including SDL_ttf to set up the textFont
#include <SDL.h>	   // including SDL to set up the SDL window
#include "iGameState.h" // including GameState so that I can create pointers and functions for the State objects


//SIMILAR TO MY GED AND RT3D (B00234203) I Removed most of the stuff from both down to a template leaving it 
// as a singleton and having Runtime loop function, setState function GetState, GetShadowDemoState and the getSDLWindow and setupRC functions

// this game class contains the run time loop and all of the instantiated states and functions to switch states
// it is currently a singleton this is largely to simplify changing states and using the various other functions 
// without having to pass in the Game object all of the time, it's also a singleton as we only ever want 1 instantiated 
// game control object at a time. 
class GameControl
{

public:
	static GameControl *getInstance();                                             // function that returns a pointer to a Game Object so we can use the functions from here(static so that its useable from anywhere without an instansiated Game object)  USED
	void run();                                                                    // function that contains the game run loop USED
	void setState(iGameState * newState);                                          // function to change the current state to the passed in GameState USED
	iGameState *getState(void){return currentState;}                               // inline function returns the currentState GameState pointer USED
	iGameState *getDemoState(void){return demoState;}								// inline function returns the ShadowDemoState GameState pointer USED			
	SDL_Window * getSDLWindow(){return hWindow;}                                   // inline function that returns the created SDL window USED
	~GameControl();                                                                // Game destructor 

private:
	GameControl();                                                         // private Game constructor so that no other class can accidently create an Game object (Singleton so there should only ever be one!) USED
	GameControl(GameControl const&){};                                     // creating my own copy constructor that is blank and private so that no copies can be created for my singleton USED
	SDL_Window * setupRC(SDL_GLContext &context);                          // creates an OpenGL context for use with SDL USED
	
	TTF_Font* textFont;													   // TTF_Font used to initalize and check that SDL_ttf is working on initilization USED
	
	static GameControl * instance;                                         // static Game pointer (for the singletons so there is only one of these) USED
	SDL_GLContext glContext;                                               // OpenGL context handle USED
	SDL_Window *hWindow;                                                    // SDL_Window type to hold the SDL_Window return from setupRC USED
      
	// GameState pointer for holding the various states USED
	iGameState * currentState;                                              
	iGameState * demoState;                                                 

	bool running;                                                          // USED

};

#endif
// Fragment Shader � file "minimal.frag"

#version 330

// passing lightPosition, world space frag position and the lights far plane
// (perspective projection)!
uniform vec4 lightPosition;
uniform float lightFarPlane;
uniform float shadowOffset;

in vec4 pos;

void main(void)
{
	
	// calculate distance between frag position and light position and add shadowOffset for this object
	// explanation of why below!
	float dist = distance(pos, lightPosition) + shadowOffset;
	
	// divide it by the far plane to get it into 0-1 values for the textures depth
	// anything over 1 should technically be out of view and thus not rendered anyway
	// we do this instead of normal depth(in this version) when using a depth cube map
	// as it makes finding the correct depths to compare much easier, if we did it the same
	// as in the normal shadow map we would have to pass all 6 view matrices of the point light
	// and then check which direction the point light needs to check to get the correct value
	// and then pick the correct view matrix and transfer the fragment position into a shadow co-ordinate
	// and then pull the value from the texture and compare it. So overall this way is easier (quicker? since 
	// other way would require a rather large amount of comparisons) downside is that we lose the benifits of 
	// polygon offset for curved biasing as we manually set the frag depth! And then have to manually set the bias
	// for the models(shadowOffset)... 
	gl_FragDepth = dist / lightFarPlane; 
}

// Phong fragment shader phong-tex.frag matched with phong-tex.vert
#version 330

// Some drivers require the following
precision highp float;

struct lightStruct
{
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
	vec4 position;
	vec3 attenuation;
	bool lightActive;
};

struct materialStruct
{
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
	float shininess;
};

uniform lightStruct light;
uniform materialStruct material;
uniform sampler2D textureUnit0;
uniform sampler2D textureUnit1;
uniform bool switchNormal;

in vec3 ex_L;
in vec3 ex_N;
in vec3 ex_V;
in vec2 ex_TexCoord;

layout(location = 0) out vec4 out_Color;
 
void main(void) {
    
	// putting rgb normal from texture space into normalized tangent space 
	vec3 normalMapVal = normalize(texture2D(textureUnit1, ex_TexCoord).xyz * 2.0 - 1.0); 
	
	// Ambient intensity, correct
	vec4 ambientI = light.ambient * material.ambient;
	vec4 diffuseI;
	vec4 specularI;
	
	// if normal is true do all the calculations with the tangent space ex_L and tangent space ex_V
	// and then the normals from the tangent space normal map
	if(switchNormal == true)
	{
		// all the calculations are essentially the same except they're done in tangent space
		// with the normal taken from the normal map, it's all per vertex calculations with standard
		// interpolation not per fragment like the other calculations

		// Diffuse intensity
		diffuseI = light.diffuse * material.diffuse;
		diffuseI = diffuseI * max(dot(normalize(normalMapVal),normalize(ex_L)),0.0);
 
		// Specular intensity
		// Calculate R - reflection of light, correct
		vec3 R = normalize(reflect(normalize(-ex_L),normalize(normalMapVal)));

		specularI = light.specular * material.specular;
		specularI = specularI * pow(max(dot(R, normalize(-ex_V)),0), material.shininess);
	
	}
	else if(switchNormal == false)// normal lighting calculations in modelview space
	{
		// Diffuse intensity
		diffuseI = light.diffuse * material.diffuse;
		diffuseI = diffuseI * max(dot(normalize(ex_N),normalize(ex_L)),0);
 
		// Specular intensity
		// Calculate R - reflection of light
		vec3 R = normalize(reflect(normalize(-ex_L),normalize(ex_N)));

		specularI = light.specular * material.specular;
		specularI = specularI * pow(max(dot(R,normalize(-ex_V)),0), material.shininess);
	}
	
	// Fragment colour 
	out_Color = (ambientI + diffuseI + specularI) * texture(textureUnit0, ex_TexCoord);
}
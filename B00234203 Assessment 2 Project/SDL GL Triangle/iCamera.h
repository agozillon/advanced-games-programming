#ifndef ICAMERA_H
#define ICAMERA_H
#include "rt3d.h"				 // including rt3d library so derived classes have access to them
#include <glm/gtc/type_ptr.hpp>  // including glm/gtc/type_ptr so that this class can use vec3s
using namespace glm;			 // using glm name space so I don't have to include variablename

// USED PREVIOUSLY IN RT3D

// abstract base class for creating cameras specifys that camera classes that inherit require get/set functions for the eye/at/up
class iCamera
{

public:
	virtual ~iCamera(){}                          // virtual inline destructor
	virtual vec3 getEye() = 0;                           // virtual function that should be implemented to return the vec3 Eye value
	virtual vec3 getAt() = 0;                            // virtual function that should be implemented to return the vec3 At value 
	virtual vec3 getUp() = 0;                            // virtual function that should be implemented to return the vec3 Up value 
	virtual void updateEye(vec3 pos) = 0;		         // virtual function that should be implemented to update the Eye position
	virtual void updateAt(vec3 pos) = 0;				 // virtual function that should be implemented to update the At position
	virtual void updateUp(vec3 pos) = 0;                 // virtual function that should be implemented to update the Up position
	virtual void updateYaw(float rot) = 0;				 // virtual function that should be implemented to update the cameras rotation
	virtual float getYaw() = 0;							 // virtual function that should be implemented to return the cameras rotation
	virtual void updatePitch(float rot) = 0;			 // virtual function that should be implemented to update the cameras rotation
	virtual float getPitch() = 0;						 // virtual function that should be implemented to return the cameras rotation	
	virtual mat4 getCurrentCamera() = 0;                 // virtual function that should be implemented to return the camera matrix
	
};

#endif
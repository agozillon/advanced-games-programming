#ifndef MD2MESH_H
#define MD2MESH_H
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <GL/glew.h>
#include "Shader.h"
#include "md2model.h"
#include "iMesh.h"
// Simple class for rendering MD2 models, similar to RT3D stuff
class MD2Mesh : public iMesh{

public:	
	MD2Mesh(const char * md2FileName, const GLuint texID, Shader * shader);
	~MD2Mesh(){delete model;}
	MD2Mesh(const char * md2FileName, const glm::vec3 pos, const glm::vec3 rot, const glm::vec3 scale, const bool castsShadow, const GLuint texID, Shader * shader);
	// basic constructor takes in files names, shader program, mesh, and the mesh index count
	void draw(glm::mat4 viewMatrix);											// draws the object with the help of a viewMatrix
	void updateShader(Shader* shader){activeShader = shader;}					// set a new active shader for the mesh
	void updateTexture(const GLuint texID){textureID = texID;}					// set a new texture ID for the mesh to use
	void updateShadowCast(const bool castsShadow){shadowCast = castsShadow;}	// set true or false for the object to cast shadows, has to be used in conjunction with shadow shader
	void updatePosition(const glm::vec3 pos){position = pos;}					
	void updateRotation(const glm::vec3 rot){rotation = rot;}
	void updateScale(const glm::vec3 scale){scalar = scale;}
	void incrementAnimation();													// increase to the next animation level
	void decreaseAnimation();													// decrease to the previous animation level
	void animateMesh();															// animate/update the md2 mesh
	glm::vec3 getPosition(){return position;}                             
	glm::vec3 getRotation(){return rotation;}
	glm::vec3 getScale(){return scalar;}

private:
	// various private variables used for holding the mesh data, orientation data and md2 model
	// related data
	md2model* model;
	int currentAnim;
	GLuint meshID;         
	GLuint textureID;
	GLuint meshVertexCount;
	Shader* activeShader;
	bool shadowCast;
	glm::vec3 position, rotation, scalar;

};

#endif
#ifndef LIGHT_H
#define LIGHT_H
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <GL/glew.h>
using namespace glm;

// light struct removed from RT3D and edited slightly
struct lightStruct {
		GLfloat ambient[4];
		GLfloat diffuse[4];
		GLfloat specular[4];
		GLfloat position[4];
		GLfloat attenuation[3];
		bool lightActive;
	};


// Simple light class, pretty much the same as RT3D minus a few useless functions
// and additional move functions and added attenuation
class Light {
public:
	// basic constructor takes the ambient, diffuse, specular and position of a Light
	Light(vec4 ambient, vec4 diffuse, vec4 specular, vec4 position, vec3 attenuation, bool lightActive);
	~Light(){}										// blank inline destructor
	lightStruct getLight(){return light;}			// returns the light structure
	void updateAmbient(vec4 ambient);			    // update ambient values
	void updateDiffuse(vec4 diffuse);				// update diffuse values
	void updateSpecular(vec4 specular);				// update specular values
	void updatePosition(vec4 position);				// update position values
	void updateAttenuation(vec3 attenuation);       // update attenuation
	void updateLightActive(bool active);
	void moveForward(float dist);                   // move light forward
	void moveRight(float dist);                     // move light right
	void moveUp(float dist);                        // move light up
	vec4 getPosition(){return vec4(light.position[0], light.position[1], light.position[2], light.position[3]);} // get light position

private:
	lightStruct light; // light structure

};

#endif
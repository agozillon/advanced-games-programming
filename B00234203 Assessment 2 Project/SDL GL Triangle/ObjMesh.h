#ifndef OBJMESH_H
#define OBJMESH_H
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <GL/glew.h>
#include "Shader.h"
#include "iMesh.h"

// Simple class for rendering objects, extremely similar to the RT3D
// ObjMeshHolder class just modified for shadow rendering, same as md2 model minus 
// md2 model relevant variables
class ObjMesh : public iMesh {

public:	
	ObjMesh(const GLuint mesh, const GLuint vertCount, const GLuint texID, Shader * shader);
	~ObjMesh(){}
	ObjMesh(const glm::vec3 pos, const glm::vec3 rot, const glm::vec3 scale, const bool castsShadow, const GLuint mesh, const GLuint vertCount, const GLuint texID, Shader * shader);
	// basic constructor takes in files names, shader program, mesh, and the mesh index count
	void draw(glm::mat4 viewMatrix);            // draws the object with the help of a viewMatrix
	void updateShader(Shader* shader){activeShader = shader;}
	void updateTexture(const GLuint texID){textureID = texID;}
	void updateShadowCast(const bool castsShadow){shadowCast = castsShadow;}
	void updatePosition(const glm::vec3 pos){position = pos;}
	void updateRotation(const glm::vec3 rot){rotation = rot;}
	void updateScale(const glm::vec3 scale){scalar = scale;}
	glm::vec3 getPosition(){return position;}
	glm::vec3 getRotation(){return rotation;}
	glm::vec3 getScale(){return scalar;}

private:
	GLuint meshID;         // ID of the mesh
	GLuint textureID;
	GLuint meshVertexCount;
	Shader* activeShader;
	bool shadowCast;
	glm::vec3 position, rotation, scalar;

};

#endif
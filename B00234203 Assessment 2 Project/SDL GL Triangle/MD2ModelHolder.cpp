#include "MD2ModelHolder.h"
#include <glm/gtc/matrix_transform.hpp> // including glm matrix_transforms for access to matrix transforms

// constructor that takes in a file name and a textureID and then creates an md2Model and loads file model file in
MD2ModelHolder::MD2ModelHolder(char *modelFileName, GLuint textureID)
{
	// setting the currentAnimation to 0, constructing the md2model class and setting the textureID
	model = new md2model();
	currentAnimation = 0;
	texture = textureID;
	
	// reading in the mesh and getting the meshIndexCount for the model
	meshObject = model->ReadMD2Model(modelFileName);
	meshIndexCount = model->getVertDataSize();
}

// destructor deleting objects instantiated within this class
MD2ModelHolder::~MD2ModelHolder()
{
	delete model;
}

// taking in a shader/material/matrix seting the material binding the texture and setting the uniform and drawing the md2model
void MD2ModelHolder::drawModel(GLuint shaderProgramID, rt3d::materialStruct materialInUse, glm::mat4 passedInMVMatrix)
{
	glCullFace(GL_FRONT);
	glBindTexture(GL_TEXTURE_2D, texture);
	rt3d::setUniformMatrix4fv(shaderProgramID, "modelview", glm::value_ptr(passedInMVMatrix));
	rt3d::setMaterial(shaderProgramID, materialInUse);
	rt3d::drawMesh(meshObject, meshIndexCount/3, GL_TRIANGLES);
	glCullFace(GL_BACK);
}

// function that animates the md2 model, and updates the mesh with new vertex data
void MD2ModelHolder::animateAndUpdateModel()
{
	model->Animate(currentAnimation,0.1);
	rt3d::updateMesh(meshObject, RT3D_VERTEX, model->getAnimVerts(), model->getVertDataSize());
}

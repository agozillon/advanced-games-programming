#ifndef RT3D
#define RT3D

#include <GL/glew.h>
#include <SDL.h>
#include <iostream>
#include <fstream>
#include <string>
#include <glm/glm.hpp>
#include <SDL_ttf.h>
#include <vector>
using namespace std;

#define RT3D_VERTEX		0
#define RT3D_COLOUR		1
#define RT3D_NORMAL		2
#define RT3D_TEXCOORD   3
#define RT3D_INDEX		4

namespace rt3d {
	void exitFatalError(const char *message);
	char* loadFile(const char *fname, GLint &fSize);
	GLuint loadBitmap(char *fname);
	GLuint textToTexture(const char * str/*, TTF_Font *font, SDL_Color colour, GLuint &w,GLuint &h */);
	// creates a depth cube map for rendering depth data to
	GLuint setupDepthCubeMap(GLuint &dCube, GLuint &framebuffer, GLuint depthCube[]);

	// Some methods for creating meshes
	// ... including one for dealing with indexed meshes
	GLuint createMesh(const GLuint numVerts, const GLfloat* vertices, const GLfloat* colours, const GLfloat* normals,
		const GLfloat* texcoords, const GLuint indexCount, const GLuint* indices);
	// these three create mesh functions simply provide more basic access to the full version
	GLuint createMesh(const GLuint numVerts, const GLfloat* vertices, const GLfloat* colours, const GLfloat* normals,
		const GLfloat* texcoords);
	GLuint createMesh(const GLuint numVerts, const GLfloat* vertices);
	GLuint createColourMesh(const GLuint numVerts, const GLfloat* vertices, const GLfloat* colours);
	
	// create a shadow buffer texture
	GLuint createBufferTexture(GLuint textureW, GLuint textureH, bool hasDepthBuffer, GLuint &depthBufferID, GLenum textureType, GLenum activeTexture);
	// creates a framebuffer and binds the texture ID to it
	GLuint bindFramebufferTexture(GLuint textureID, GLenum textureAttach, GLenum drawBufferMode);

	// function for creating tangent data for meshes
	void createTangents(int vertexCount, vector<GLfloat> vertices,  vector<GLfloat> norms, vector<GLfloat> texCoords, vector<GLuint> indices, vector<GLfloat> &tangents);
	
	// checks for GL errors
	int checkGLErrors();

	void drawMesh(const GLuint mesh, const GLuint numVerts, const GLuint primitive); 
	void drawIndexedMesh(const GLuint mesh, const GLuint indexCount, const GLuint primitive);
	
	void updateMesh(const GLuint mesh, const unsigned int bufferType, const GLfloat *data, const GLuint size);
}

#endif
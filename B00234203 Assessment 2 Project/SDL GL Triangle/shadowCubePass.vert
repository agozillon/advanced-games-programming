// Vertex Shader � file "minimal.vert"

#version 330

// the usual uniforms with an extra model matrix passed in
// may be more efficent in the future to change it to model, projection and view
// instead of a combined (even if having a precomputed MVP saves GPU cycles?..dunno)
uniform mat4 projection;
uniform mat4 modelview;
uniform mat4 model;

in  vec3 in_Position;
out vec4 pos;

void main(void)
{
	// standard MVP clipspace position output
	gl_Position = projection * modelview * vec4(in_Position, 1.0);
	
	// passing position in world space to get the distance between the frag and the
	// light in the correct space
	pos = model * vec4(in_Position, 1.0);
}
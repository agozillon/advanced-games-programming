#include "cDemoState.h"
#include "GameControl.h"
#include "rt3d.h"
#include "rt3dObjLoader.h"
#include <vector>
#include <stack>

void cDemoState::enter()
{

}

// basically binds the shadow map frame buffer and renders the scene from 
// the  shadow map lights point of view
void cDemoState::shadowMapPass()
{
	
	glBindFramebuffer(GL_FRAMEBUFFER, framebufferID); // binding to the framebuffer I created
	glViewport(0, 0, 1024, 1024);                     // setting viewport to the textures resolution
	
	// clear the current frame buffers color and depth
	glClearColor(0.5f,0.5f,0.5f,1.0f);                 
	glClear(GL_COLOR_BUFFER_BIT  | GL_DEPTH_BUFFER_BIT);

	// enabling culling of the front faces instead of the back, this helps stop z-buffer fighting between the depth values and fragment values in light space(Shadow Acne)
	// as the depth value will no longer be so close to the currently being drawn frag value that it causes the depth test to fail due to precision errors
	glEnable(GL_CULL_FACE);
	glCullFace(GL_FRONT);
	// similar to switching front face culling this can help stop shadow acne by offsetting the polygon values in the depth map by a certain amount
	glEnable(GL_POLYGON_OFFSET_FILL);
	glPolygonOffset(1.0, 2.0);

	// setting projection matrix to depth map resolution
	glm::mat4 projection = glm::perspective(90.0f,1024.0f/1024.0f,1.0f, 100.0f);

	//setting up lights view matrix
	glm::mat4 lightViewMatrix = glm::lookAt(glm::vec3(sceneShadowMapLight->getPosition().x, sceneShadowMapLight->getPosition().y, sceneShadowMapLight->getPosition().z), glm::vec3(0, 0, 0), glm::vec3(1, 0, 0));
	
	// matrice required to move world co-ordinate positions from clip space co-ordinates in the -1 to 1 range
	// to the 0 - 1 range that when combined with the perspective divide can be used to access the shadow map and check for depth
	glm::mat4 biasMatrix = glm::mat4(0.5f, 0, 0, 0.0f, 0, 0.5f, 0, 0.0f, 0, 0, 0.5f, 0.0f, 0.5, 0.5, 0.5, 1.0f);
	
	// sending the shadow matrix to the shadow shader, bias * projection * view
	shaderManager->getShader("Shadow Map")->setActiveShader();
	shaderManager->getShader("Shadow Map")->setUniformMatrix4fv("biasMatrix", glm::value_ptr(biasMatrix));		
	shaderManager->getShader("Shadow Map")->setUniformMatrix4fv("lightView", glm::value_ptr(lightViewMatrix));		
	shaderManager->getShader("Shadow Map")->setUniformMatrix4fv("lightProjection", glm::value_ptr(projection));		
	
	// setting shader to the simple shader and passing the projection matrix
	shaderManager->getShader("Simple")->setActiveShader();
	shaderManager->getShader("Simple")->setUniformMatrix4fv("projection", glm::value_ptr(projection));

	// rendering all the objects I wish to cast shadows

	// floating cube
	objMeshList[3]->updateShader(shaderManager->getShader("Simple"));
	objMeshList[3]->draw(lightViewMatrix);
		
	//spaceship 
	objMeshList[2]->updateShader(shaderManager->getShader("Simple"));
	objMeshList[2]->draw(lightViewMatrix);
	
	// sphere
	objMeshList[0]->updateShader(shaderManager->getShader("Simple"));
	objMeshList[0]->draw(lightViewMatrix);

	// Normal cube, can cast shadow doesn't have shadows cast onto it
	objMeshList[4]->updateShader(shaderManager->getShader("Simple"));
	objMeshList[4]->draw(lightViewMatrix);
	
	// Normal cube walls, can cast shadows doesn't have shadows cast on them!
	objMeshList[5]->updateShader(shaderManager->getShader("Simple"));
	objMeshList[5]->updatePosition(glm::vec3(-10.0, 5.0, 11.5));
	objMeshList[5]->draw(lightViewMatrix);
	
	objMeshList[5]->updatePosition(glm::vec3(-31.5, 5.0, -10.0));
	objMeshList[5]->updateRotation(glm::vec3(0, 90, 0));
	objMeshList[5]->draw(lightViewMatrix);

	objMeshList[5]->updatePosition(glm::vec3(11.5, 5.0, -10.0));
	objMeshList[5]->updateRotation(glm::vec3(0, 90, 0));
	objMeshList[5]->draw(lightViewMatrix);

	objMeshList[5]->updatePosition(glm::vec3(-10.0, 5.0, -31.5));
	objMeshList[5]->updateRotation(glm::vec3(0, 0, 0));
	objMeshList[5]->draw(lightViewMatrix);
	
	// draw the hobgoblin
	glCullFace(GL_FRONT); // md2 faces are defined clockwise, so cull front face
	md2Model->updateShader(shaderManager->getShader("Simple"));
	md2Model->draw(lightViewMatrix);
	glCullFace(GL_BACK);

	// similar to switching front face culling this can help stop shadow acne by offsetting the polygon values in the depth map by a certain amount
	glDisable(GL_POLYGON_OFFSET_FILL);
}

// similar to the shadow map pass except that it binds a seperate framebuffer with
// the cube map bound to it and having depths rendered to! 
void cDemoState::shadowCubeMapPass()
{
	// bind framebuffer setup viewport for rendering one side of the shadow cube map
	glBindFramebuffer(GL_FRAMEBUFFER, framebuffer2ID);
	glViewport(0, 0, 1024, 1024);

	// set active shader to SimpleCubePass which renders the depth to the texture
	// works slightly different than the simple shader
	shaderManager->getShader("SimpleCubePass")->setActiveShader();
	
	// setting projection matrix to one side of the depth cube maps resolution
	glm::mat4 projection(1.0);
	projection = glm::perspective(90.0f,1024.0f/1024.0f,1.0f,100.0f);
	shaderManager->getShader("SimpleCubePass")->setUniformMatrix4fv("projection", glm::value_ptr(projection));

	
	
	// rendering the following meshes to the depth cube map using the various
	// light view matrices
	for(int i = 0; i < 6; i++)
	{	
		// bind current loops side to the buffer
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, depthSides[i], depthCube, 0);
	
		// clear the current frame buffer
		glClearColor(0.5f,0.5f,0.5f,1.0f);
		glClear(GL_COLOR_BUFFER_BIT  | GL_DEPTH_BUFFER_BIT);
		// can set the buffer to cull front faces and offset to avoid Z-fighting between shadowed parts and colored frags glEnable(GL_POLYGON_OFFSET_FILL); glPolygonOffset(1.0f, 2.0f);
		glEnable(GL_CULL_FACE);
		glCullFace(GL_FRONT);
		

		// cube for ground plane, setting up shader and passing the offset i wish for this model
		// in replace of GL_POLYGON_OFFSET_FILL which doesn't work for this shader as we overwrite 
		// the depth!
		objMeshList[1]->updateShader(shaderManager->getShader("SimpleCubePass"));
		shaderManager->getShader("SimpleCubePass")->setUniformFloat("shadowOffset", 0.0);
		objMeshList[1]->draw(lightViewMatrices[i]);
		
		
		// draw a cube block on top of ground plane
		objMeshList[3]->updateShader(shaderManager->getShader("SimpleCubePass"));
		shaderManager->getShader("SimpleCubePass")->setUniformFloat("shadowOffset", 1.0);
		objMeshList[3]->draw(lightViewMatrices[i]);
	
		// reversing the culling for the md2 model since we cull front faces for it during the
		// actual render process anyway, reduces shadow acne on model still present however.
		// draw the hobgoblin
		glCullFace(GL_BACK); // md2 faces are defined clockwise, so cull front face
		md2Model->updateShader(shaderManager->getShader("SimpleCubePass"));
		shaderManager->getShader("SimpleCubePass")->setUniformFloat("shadowOffset", 0.0);
		md2Model->draw(lightViewMatrices[i]);
		glCullFace(GL_FRONT);
	
		// spaceship 
		objMeshList[2]->updateShader(shaderManager->getShader("SimpleCubePass"));
		shaderManager->getShader("SimpleCubePass")->setUniformFloat("shadowOffset", 0.0);
		objMeshList[2]->draw(lightViewMatrices[i]);
	

		// sphere
		objMeshList[0]->updateShader(shaderManager->getShader("SimpleCubePass"));
		shaderManager->getShader("SimpleCubePass")->setUniformFloat("shadowOffset", 0.25);
		objMeshList[0]->draw(lightViewMatrices[i]);
		
		
		// Normal cube, can cast shadow doesn't have shadows cast onto it
		objMeshList[4]->updateShader(shaderManager->getShader("SimpleCubePass"));
		shaderManager->getShader("SimpleCubePass")->setUniformFloat("shadowOffset", 0.30);
		objMeshList[4]->draw(lightViewMatrices[i]);

		// Normal cube walls, can cast shadow doesn't have shadows cast onto it
		objMeshList[5]->updateShader(shaderManager->getShader("SimpleCubePass"));
		objMeshList[5]->updatePosition(glm::vec3(-10.0, 5.0, 11.5));
		objMeshList[5]->draw(lightViewMatrices[i]);
	
		objMeshList[5]->updatePosition(glm::vec3(-31.5, 5.0, -10.0));
		objMeshList[5]->updateRotation(glm::vec3(0, 90, 0));
		objMeshList[5]->draw(lightViewMatrices[i]);

		objMeshList[5]->updatePosition(glm::vec3(11.5, 5.0, -10.0));
		objMeshList[5]->updateRotation(glm::vec3(0, 90, 0));
		objMeshList[5]->draw(lightViewMatrices[i]);

		objMeshList[5]->updatePosition(glm::vec3(-10.0, 5.0, -31.5));
		objMeshList[5]->updateRotation(glm::vec3(0, 0, 0));
		objMeshList[5]->draw(lightViewMatrices[i]);
	}

}

void cDemoState::init()
{
	// Instantiating the first person camera, model and texture handler and shadermanager
	camera = new FirstPersonCamera();
	modelAndTextureHandler = new ObjModelAndTextureHandler();
	shaderManager = new ShaderManager();

	////////////////////////////////
	///// LIGHTS AND MATERIALS /////
	////////////////////////////////
	numberOfScenePointLights = 8;
	lightArrayNames[0] = "lights[0]";
	scenePointLights[0] = new Light(glm::vec4(0.02, 0.0, 0.0, 1.0), glm::vec4(0.2, 0.0, 0.0, 1.0), glm::vec4(0.1, 0.0, 0.0, 1.0), glm::vec4(0.0, 20.0, 0.0, 1.0), glm::vec3(1.0000f, 0.0001f, 0.0001f), true);
	lightArrayNames[1] = "lights[1]"; 
	scenePointLights[1] = new Light(glm::vec4(0.0, 0.02, 0.0, 1.0), glm::vec4(0.0, 0.2, 0.0, 1.0), glm::vec4(0.0, 0.1, 0.0, 1.0), glm::vec4(0.0, 20.0, 0.0, 1.0), glm::vec3(1.0000f, 0.0001f, 0.0001f), true);
	lightArrayNames[2] = "lights[2]"; 
	scenePointLights[2] = new Light(glm::vec4(0.0, 0.0, 0.02, 1.0), glm::vec4(0.0, 0.0, 0.2, 1.0), glm::vec4(0.0, 0.0, 0.1, 1.0), glm::vec4(0.0, 20.0, 0.0, 1.0), glm::vec3(1.0000f, 0.0001f, 0.0001f), true);
	lightArrayNames[3] = "lights[3]";
	scenePointLights[3] = new Light(glm::vec4(0.02, 0.02, 0.0, 1.0), glm::vec4(0.1, 0.1, 0.0, 1.0), glm::vec4(0.1, 0.1, 0.0, 1.0), glm::vec4(0.0, 20.0, 0.0, 1.0), glm::vec3(1.0000f, 0.0001f, 0.0001f), true);
	lightArrayNames[4] = "lights[4]";
	scenePointLights[4] = new Light(glm::vec4(0.0, 0.02, 0.02, 1.0), glm::vec4(0.0, 0.1, 0.1, 1.0), glm::vec4(0.0, 0.1, 0.1, 1.0), glm::vec4(0.0, 20.0, 0.0, 1.0), glm::vec3(1.0000f, 0.0001f, 0.0001f), true);
	lightArrayNames[5] = "lights[5]";
	scenePointLights[5] = new Light(glm::vec4(0.02, 0.0, 0.02, 1.0), glm::vec4(0.1, 0.0, 0.1, 1.0), glm::vec4(0.1, 0.0, 0.1, 1.0), glm::vec4(0.0, 20.0, 0.0, 1.0), glm::vec3(1.0000f, 0.0001f, 0.0001f), true);
	lightArrayNames[6] = "lights[6]";
	scenePointLights[6] = new Light(glm::vec4(0.02, 0.02, 0.02, 1.0), glm::vec4(0.2, 0.1, 0.3, 1.0), glm::vec4(0.2, 0.1, 0.3, 1.0), glm::vec4(0.0, 20.0, 0.0, 1.0), glm::vec3(1.0000f, 0.0001f, 0.0001f), true);
	lightArrayNames[7] = "lights[7]";
	scenePointLights[7] = new Light(glm::vec4(0.02, 0.02, 0.02, 1.0), glm::vec4(0.2, 0.3, 0.1, 1.0), glm::vec4(0.2, 0.3, 0.1, 1.0), glm::vec4(0.0, 20.0, 0.0, 1.0), glm::vec3(1.0000f, 0.0001f, 0.0001f), true);
	
	sceneShadowMapLight = new Light(glm::vec4(0.5, 0.5, 0.5, 1.0), glm::vec4(1.0, 1.0, 1.0, 1.0), glm::vec4(0.3, 0.3, 0.3, 1.0), glm::vec4(-1.0, 30.0, 0.0, 1.0), glm::vec3(1.0, 0.0001f, 0.0001f), true);
	sceneMaterial = new Material(glm::vec4(0.5f, 0.5f, 0.5f, 1.0f), glm::vec4(1.0f, 1.0f, 1.0f, 1.0f), glm::vec4(0.8f, 0.8f, 0.8f, 1.0), 2.0f);
	
	// setting to true so the scene starts in omni/point light mode	
	omniLightActive = true;
	lightRotation = true;
	delayCounter = 31;

	// setup the default attribute list, ones normally specified in the RT3D lib
	shaderManager->addDefaultAttributes();
	
	// various starting angles to calculate the point lights position in there rotation
	angle[0] = 0;
	angle[1] = 40;
	angle[2] = 80;
	angle[3] = 120;
	angle[4] = 160;
	angle[5] = 200;
	angle[6] = 240;
	angle[7] = 280;

	////////////////////////////
	///// NORMAL MAPPING   /////
	////////////////////////////
	shaderManager->addAttribute(4, "in_Tangent"); // adding a tangent attribute for the Normal map shader
	shaderManager->createShaderProgram("Normal Map", "normalmapping.vert", "normalmapping.frag");
	shaderManager->getShader("Normal Map")->setActiveShader();	
	shaderManager->getShader("Normal Map")->setUniformMatrix4fv("projection", glm::value_ptr(glm::perspective(60.0f,800.0f/600.0f,1.0f,150.0f)));
	shaderManager->getShader("Normal Map")->setLight(sceneShadowMapLight->getLight());
	shaderManager->getShader("Normal Map")->setMaterial(sceneMaterial->getMaterial());
	shaderManager->getShader("Normal Map")->setUniformi("textureUnit0", 0);
	shaderManager->getShader("Normal Map")->setUniformi("textureUnit1", 1);
	normalMapSwitch = 1;
	shaderManager->getShader("Normal Map")->setUniformi("switchNormal", normalMapSwitch);
	
	/////////////////////////////////////////
	/////SIMPLE CUBE SHADOW PASS SHADER /////
	/////////////////////////////////////////
	//omni directional shadow related stuff
	shaderManager->createShaderProgram("SimpleCubePass",  "shadowCubePass.vert", "shadowCubePass.frag");
	shaderManager->getShader("SimpleCubePass")->setActiveShader();
	shaderManager->getShader("SimpleCubePass")->setUniformFloat("lightFarPlane", 100.0);
	shaderManager->getShader("SimpleCubePass")->setUniformVec4fv("lightPosition", glm::value_ptr(scenePointLights[0]->getPosition()));
	
	////////////////////////////////////////////
	/////OMNI-DIRECTIONAL SHADOW MAP SHADER/////
	////////////////////////////////////////////
	shaderManager->createShaderProgram("Combined Shader", "CombinedShader.vert", "CombinedShader.frag");
	shaderManager->getShader("Combined Shader")->setActiveShader();
	shaderManager->getShader("Combined Shader")->setUniformFloat("lightFarPlane", 100.0);
	shaderManager->getShader("Combined Shader")->setMaterial(sceneMaterial->getMaterial());
    shaderManager->getShader("Combined Shader")->setUniformi("textureUnit0", 0);
	shaderManager->getShader("Combined Shader")->setUniformi("textureUnit1", 1);
	shaderManager->getShader("Combined Shader")->setUniformi("textureUnit2", 2);
	// not the ideal place to bind a normal texutre, however since I'm only using one right now
	// it doesn't make sense to constantly re-bind during draw
	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, normalTexture);
	glActiveTexture(GL_TEXTURE0);

	// sending array of light structures to the shader, one per light
	lightStruct tempLightArray[8];
	for(int i = 0; i < numberOfScenePointLights; i++)
	{
		tempLightArray[i] = scenePointLights[i]->getLight();
	}
	shaderManager->getShader("Combined Shader")->setLights(tempLightArray, numberOfScenePointLights, lightArrayNames);
	
	// setting up the cubemaps sides for easy loop through on creation
	depthSides[0] = GL_TEXTURE_CUBE_MAP_POSITIVE_X; depthSides[1] = GL_TEXTURE_CUBE_MAP_NEGATIVE_X;
	depthSides[2] = GL_TEXTURE_CUBE_MAP_POSITIVE_Y; depthSides[3] =	GL_TEXTURE_CUBE_MAP_NEGATIVE_Y;
	depthSides[4] = GL_TEXTURE_CUBE_MAP_POSITIVE_Z;	depthSides[5] = GL_TEXTURE_CUBE_MAP_NEGATIVE_Z;
	
	// calling to setup the depth cube map
	rt3d::setupDepthCubeMap(depthCube, framebuffer2ID, depthSides);
	shaderManager->clearAttributes();
	shaderManager->addDefaultAttributes();

	////////////////////////////
	/////   TEXTURED SHADE /////
	////////////////////////////
	// loading in shaders and setting up uniforms that won't require any changing throughout 
	shaderManager->createShaderProgram("Textured" ,"textured.vert", "textured.frag");	
	// setting projection for texturedProgram, just the standard draw to screen with texture
	shaderManager->getShader("Textured")->setActiveShader();
	shaderManager->getShader("Textured")->setUniformMatrix4fv("projection", glm::value_ptr(glm::perspective(60.0f,800.0f/600.0f,1.0f,150.0f)));
	
	////////////////////////////
	/////   BASIC SHADER   /////
	////////////////////////////
	// simple shader no uniforms need passed
	shaderManager->createShaderProgram("Simple" ,"mvp.vert", "minimal.frag");	
	
	////////////////////////////
	/////   SHADOW MAPPING /////
	////////////////////////////
	// setting up all of our shadowMapProgram's various requirements
	shaderManager->createShaderProgram("Shadow Map", "shadowmap.vert", "shadowmap.frag"); 
	shaderManager->getShader("Shadow Map")->setActiveShader();
	shaderManager->getShader("Shadow Map")->setLight(sceneShadowMapLight->getLight());
	shaderManager->getShader("Shadow Map")->setMaterial(sceneMaterial->getMaterial());
	filterValue = 1;
	// explicitly setting the texture units to 0 and 1 by passing them a uniform integer
	// that changes there texture units. good for stoppping any kind of texture messups or mismatches
	shaderManager->getShader("Shadow Map")->setUniformi("textureUnit0", 0);
	shaderManager->getShader("Shadow Map")->setUniformi("textureUnit1", 1);
	shaderManager->getShader("Shadow Map")->setUniformi("filterMode", filterValue);
		
	// setting up the shadow map texture buffer
	GLuint tmpReference = 0;
	shadowMapID = rt3d::createBufferTexture(1024, 1024, false, tmpReference, GL_DEPTH_COMPONENT, GL_TEXTURE1);
	
	// creating framebuffer and binding the previously made texture to it. 
	framebufferID = rt3d::bindFramebufferTexture(shadowMapID, GL_DEPTH_ATTACHMENT, GL_NONE);

	
	////////////////////////////
	///MESH AND TEXTURE SETUP///
	////////////////////////////

	// loading in and collecting textures
	modelAndTextureHandler->loadAndStoreTexture("fabric.bmp");
	modelAndTextureHandler->loadAndStoreTexture("hobgoblin2.bmp");
	modelAndTextureHandler->loadAndStoreTexture("studdedmetal.bmp");
	modelAndTextureHandler->loadAndStoreTexture("MetallicAssembly-ColorMap.bmp");
	modelAndTextureHandler->loadAndStoreTexture("MetallicAssembly-NormalMap.bmp");
	modelAndTextureHandler->loadAndStoreTexture("space_frigate_6_color.bmp");

	// loading in obj model files and setting them up, false if we wish to create tangents for it, true if we want tangents created!
	modelAndTextureHandler->loadAndStoreObjModel("cube.obj", true);
	modelAndTextureHandler->loadAndStoreObjModel("space_frigate.obj", false);
	modelAndTextureHandler->loadAndStoreObjModel("sphere.obj", false);
	
	GLuint tempTexture;
	GLuint tempIndex;
	GLuint tempMesh;
	GLuint tempNormalTexture;
	
	numberOfObjMesh = 6;
	// sphere
	modelAndTextureHandler->getObjModel(tempMesh, tempIndex, "sphere.obj");
	objMeshList[0] = new ObjMesh(glm::vec3(-10.0, 5.0, -10), glm::vec3(0, 0, 0), glm::vec3(1, 1, 1), true, tempMesh, tempIndex, 0, shaderManager->getShader("Combined Shader"));
	// ground plane
	modelAndTextureHandler->getObjModel(tempMesh, tempIndex, "cube.obj");
	modelAndTextureHandler->getTextureID(tempTexture, "studdedmetal.bmp");
	objMeshList[1] = new ObjMesh(glm::vec3(-10, -0.1, -10), glm::vec3(0, 0, 0), glm::vec3(20, 0.1, 20), true, tempMesh, tempIndex, tempTexture, shaderManager->getShader("Combined Shader"));
	// space frigate 
	modelAndTextureHandler->getObjModel(tempMesh, tempIndex, "space_frigate.obj");
	modelAndTextureHandler->getTextureID(tempTexture, "space_frigate_6_color.bmp");
	objMeshList[2] = new ObjMesh(glm::vec3(-15, 2.5, 0), glm::vec3(0, 0, 0), glm::vec3(0.5, 0.5, 0.5), true, tempMesh, tempIndex, tempTexture, shaderManager->getShader("Combined Shader"));
	// floating cube
	modelAndTextureHandler->getObjModel(tempMesh, tempIndex, "cube.obj");
	modelAndTextureHandler->getTextureID(tempTexture, "fabric.bmp");
	objMeshList[3] = new ObjMesh(glm::vec3(0.0, 2.0, -7.0), glm::vec3(0, 0, 0), glm::vec3(1, 1, 1), true, tempMesh, tempIndex, tempTexture, shaderManager->getShader("Combined Shader"));
	//  Normal map cube rotating version
	modelAndTextureHandler->getTextureID(tempTexture, "MetallicAssembly-ColorMap.bmp");
	modelAndTextureHandler->getTextureID(normalTexture, "MetallicAssembly-NormalMap.bmp");
	objMeshList[4] = new ObjMesh(glm::vec3(0.0, 2.0, 0.0), glm::vec3(0, 0, 0), glm::vec3(1, 1, 1), true, tempMesh, tempIndex, tempTexture, shaderManager->getShader("Normal Map"));
	// Normal map cube walls
	objMeshList[5] = new ObjMesh(glm::vec3(-10.0, 5.0, -31.5), glm::vec3(0, 0, 0), glm::vec3(20, 5.0, 1.5), true, tempMesh, tempIndex, tempTexture, shaderManager->getShader("Normal Map"));
	// tris md2 model
	modelAndTextureHandler->getTextureID(tempTexture, "hobgoblin2.bmp");
	md2Model = new MD2Mesh("tris.md2", glm::vec3(1.0f, 1.2f, -5.0f), glm::vec3(-90.0f, 0.0f, 0.0f), glm::vec3(0.05, 0.05, 0.05), true, tempTexture, shaderManager->getShader("Combined Shader"));

	////////////////////////////
	/////  SKYBOX SHADER   /////
	////////////////////////////
	// loading in skybox program and setting it up
		const char *cubeTexFiles[6] = {"skybox/skyhback.bmp", "skybox/skyhfront.bmp",
	"skybox/skyhleft.bmp", "skybox/skyhright.bmp",
	"skybox/uphback.bmp", "skybox/downhback.bmp" };

	shaderManager->createShaderProgram("Skybox","cubemap.vert","cubemap.frag");
	shaderManager->getShader("Skybox")->setActiveShader();
	
	// setting up array of texture names and passing them to the CubeMap class to load them in
	// and set them up, alongside a cube mesh and the shader program I wish to use.
	testCMap = new SkyCubeMap(cubeTexFiles, shaderManager->getShader("Skybox"), tempMesh, tempIndex);
	
	// setting up the various depth tests, and blend options as well as setting the first active shader
	shaderManager->getShader("Combined Shader")->setActiveShader();
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
	glDepthMask(GL_TRUE);
}

void cDemoState::draw()
{
	// rendering the shadows maps dependant on which type of light is active
	if(omniLightActive == true)
	{
		shadowCubeMapPass();
	}
	else
		shadowMapPass();

	// bind default framebuffer, set correct viewport size, clear the buffers
	// and set culling to the normal back culling for proper rendering
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glViewport(0, 0, 800, 600);
	glClearColor(0.5f,0.5f,0.5f,1.0f);
	glClear(GL_COLOR_BUFFER_BIT  | GL_DEPTH_BUFFER_BIT);
	glCullFace(GL_BACK); 

	// setting up perspetive projection correctly
	glm::mat4 projection(1.0);
	projection = glm::perspective(60.0f,800.0f/600.0f,1.0f,150.0f);
	
	// draw the skybox, setup uniform projection
	glActiveTexture(GL_TEXTURE0);
	shaderManager->getShader("Skybox")->setActiveShader();
	shaderManager->getShader("Skybox")->setUniformMatrix4fv("projection", glm::value_ptr(projection));
	testCMap->draw(camera->getCurrentCamera());

	glCullFace(GL_BACK); // make sure backface is on after rendering the skybox
	glDepthMask(GL_TRUE); // make sure depth test is on

	// activate the second texture unit to send the shadow/cube shadow maps 
	glActiveTexture(GL_TEXTURE1);
	
	// set the shader dependant on if the omnilight active shader is on or not.
	if(omniLightActive == true)
	{
		shaderManager->getShader("Combined Shader")->setActiveShader();
	}
	else
	{
		shaderManager->getShader("Shadow Map")->setActiveShader();
	}

	// binding and activating the shadowMapID/depthcube to texture location 1, and also sending the view matrix to the omni shadow shader
	if(omniLightActive == true)
	{
		glBindTexture(GL_TEXTURE_CUBE_MAP, depthCube);
		shaderManager->getShader("Combined Shader")->setUniformMatrix4fv("projection", glm::value_ptr(projection));
		shaderManager->getShader("Combined Shader")->setUniformMatrix4fv("viewMatrix", glm::value_ptr(camera->getCurrentCamera()));
		shaderManager->getShader("Combined Shader")->setUniformi("normalMapped", false);
		glActiveTexture(GL_TEXTURE2);
		glBindTexture(GL_TEXTURE_2D, normalTexture);
	}
	else
	{
		glBindTexture(GL_TEXTURE_2D, shadowMapID);
		shaderManager->getShader("Shadow Map")->setUniformMatrix4fv("projection", glm::value_ptr(projection));
	}
	
	// render the cube and set the desired shadow shader to render it with
	glActiveTexture(GL_TEXTURE0);
	if(omniLightActive == true)
	{
		objMeshList[3]->updateShader(shaderManager->getShader("Combined Shader"));
	}
	else
		objMeshList[3]->updateShader(shaderManager->getShader("Shadow Map"));
	
	objMeshList[3]->draw(camera->getCurrentCamera());

	// draw the hobgoblin, md2 model triangles are defined clockwise so must cull different face
	// to render the model properly
	glCullFace(GL_FRONT);
	if(omniLightActive == true)
	{
		md2Model->updateShader(shaderManager->getShader("Combined Shader"));
	}
	else
		md2Model->updateShader(shaderManager->getShader("Shadow Map"));
	
	md2Model->draw(camera->getCurrentCamera());
	glCullFace(GL_BACK);

	// render the ground plane and set the desired shadow shader to render it with
	if(omniLightActive == true)
	{
		objMeshList[1]->updateShader(shaderManager->getShader("Combined Shader"));
	}
	else
		objMeshList[1]->updateShader(shaderManager->getShader("Shadow Map"));
	
	objMeshList[1]->draw(camera->getCurrentCamera());

	// draw the TECHNO SPHERE!
	if(omniLightActive == true)
	{
		objMeshList[0]->updateShader(shaderManager->getShader("Combined Shader"));
	}
	else
		objMeshList[0]->updateShader(shaderManager->getShader("Shadow Map"));
	
	objMeshList[0]->draw(camera->getCurrentCamera());

	// render the cube and set the desired shadow shader to render it with
	if(omniLightActive == true)
	{
		objMeshList[2]->updateShader(shaderManager->getShader("Combined Shader"));
	}
	else
		objMeshList[2]->updateShader(shaderManager->getShader("Shadow Map"));

	objMeshList[2]->draw(camera->getCurrentCamera());
	
	
	if(omniLightActive == true)
	{
		shaderManager->getShader("Textured")->setActiveShader();

		// scaling, positioning and drawing the cube mesh to display the various point lights positions
		for(int i = 0; i < numberOfScenePointLights; i++)
		{
			objMeshList[3]->updatePosition(glm::vec3(scenePointLights[i]->getPosition().x, scenePointLights[i]->getPosition().y, scenePointLights[i]->getPosition().z));
			objMeshList[3]->updateScale(glm::vec3(0.50, 0.50, 0.50));
			objMeshList[3]->updateShader(shaderManager->getShader("Textured"));
			objMeshList[3]->draw(camera->getCurrentCamera());
		}
	}
	else
	{
		shaderManager->getShader("Textured")->setActiveShader();
		objMeshList[3]->updatePosition(glm::vec3(sceneShadowMapLight->getPosition().x, sceneShadowMapLight->getPosition().y, sceneShadowMapLight->getPosition().z));
		objMeshList[3]->updateScale(glm::vec3(0.50, 0.50, 0.50));
		objMeshList[3]->updateShader(shaderManager->getShader("Textured"));
		objMeshList[3]->draw(camera->getCurrentCamera());
	}
	
	// setting the cube back to it's original position
	objMeshList[3]->updatePosition(glm::vec3(0.0, 2.0, -7.0));
	objMeshList[3]->updateScale(glm::vec3(1.0, 1.0, 1.0));

	// setting the nomral map shader as the currently active shader passing the view matrix to the shader
	// then passing the normal map to the shader and drawing the rotating cube and the walls!
	shaderManager->getShader("Normal Map")->setActiveShader();
	objMeshList[4]->updateShader(shaderManager->getShader("Normal Map"));
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, normalTexture);
	objMeshList[4]->draw(camera->getCurrentCamera());
	glActiveTexture(GL_TEXTURE0);

	// draws the walls normal mapped within both the combined shader and Normal Shader
	// dependant on the scene currently being rendered!
	if(omniLightActive == true)
	{
		shaderManager->getShader("Combined Shader")->setActiveShader();
		shaderManager->getShader("Combined Shader")->setUniformi("normalMapped", true);
		objMeshList[5]->updateShader(shaderManager->getShader("Combined Shader"));
	}
	else
	{
		shaderManager->getShader("Normal Map")->setActiveShader();
		objMeshList[5]->updateShader(shaderManager->getShader("Normal Map"));
	}

	objMeshList[5]->updatePosition(glm::vec3(-10.0, 5.0, 11.5));
	objMeshList[5]->draw(camera->getCurrentCamera());
	
	objMeshList[5]->updatePosition(glm::vec3(-31.5, 5.0, -10.0));
	objMeshList[5]->updateRotation(glm::vec3(0, 90, 0));
	objMeshList[5]->draw(camera->getCurrentCamera());

	objMeshList[5]->updatePosition(glm::vec3(11.5, 5.0, -10.0));
	objMeshList[5]->updateRotation(glm::vec3(0, 90, 0));
	objMeshList[5]->draw(camera->getCurrentCamera());

	objMeshList[5]->updatePosition(glm::vec3(-10.0, 5.0, -31.5));
	objMeshList[5]->updateRotation(glm::vec3(0, 0, 0));
	objMeshList[5]->draw(camera->getCurrentCamera());
	
    SDL_GL_SwapWindow(GameControl::getInstance()->getSDLWindow()); // swap buffers
}

void cDemoState::update()
{
	// Added a delay counter to the key presses as it's a pain in the neck when you press
	// it once and it updates so fast that certain switches switch values to fast due to loopign
	// through several times when the key is down! This is the incrementer and specific key presses
	// reset it to 0 and check for when it's above 30
	delayCounter += 1;

	// keys for chaning the cameras position and rotation
	const Uint8 *keys = SDL_GetKeyboardState(NULL);
	if ( keys[SDL_SCANCODE_W] ) camera->moveForwardOrBackwards(0.1f);
	if ( keys[SDL_SCANCODE_S] ) camera->moveForwardOrBackwards(-0.1f);
	if ( keys[SDL_SCANCODE_A] ) camera->moveRightOrLeft(-0.1f);
	if ( keys[SDL_SCANCODE_D] ) camera->moveRightOrLeft(0.1f);
	if ( keys[SDL_SCANCODE_R] ) camera->moveUpOrDown(0.1);
	if ( keys[SDL_SCANCODE_F] ) camera->moveUpOrDown(-0.1);

	if( keys[SDL_SCANCODE_UP]) camera->updatePitch(1.0);
	if( keys[SDL_SCANCODE_DOWN]) camera->updatePitch(-1.0);
	if( keys[SDL_SCANCODE_RIGHT] || keys[SDL_SCANCODE_PERIOD]) camera->updateYaw(1.0);
	if( keys[SDL_SCANCODE_LEFT] || keys[SDL_SCANCODE_COMMA]) camera->updateYaw(-1.0);

	// key to switch between currently selected light which essentially allows
	// you to move either the omni shadow light or normal shadow light
	if( keys[SDL_SCANCODE_E] && delayCounter > 30){
		delayCounter = 0;
		if(omniLightActive == true)
			omniLightActive = false;
		else
			omniLightActive = true;
	}
	
	// turn normal mapping on and off
	if( keys[SDL_SCANCODE_N] && delayCounter > 30){
	
		delayCounter = 0;

		if(normalMapSwitch == 0)
		{
			normalMapSwitch = 1;
			std::cout << "Normal Mapping On" << std::endl;
		}
		else
		{
			normalMapSwitch = 0;
			std::cout << "Normal Mapping Off" << std::endl;
		}

		shaderManager->getShader("Normal Map")->setActiveShader();
		shaderManager->getShader("Normal Map")->setUniformi("switchNormal", normalMapSwitch);
	}

	// change the shadow map filter mode through basic shadow mapping, to PCF and then into poisson sampling
	if ( keys[SDL_SCANCODE_1] && omniLightActive == false && delayCounter > 30 ) {
	
		shaderManager->getShader("Shadow Map")->setActiveShader();

		delayCounter = 0;

		filterValue += 1;

		if(filterValue > 2)
			filterValue = 0;

		if(filterValue == 0)
			std::cout << "Basic Shadow Filter: Just Checks if something is in shadow! And linearly checks nearby since it's a Shadow Sampler" << std::endl;	
		else if(filterValue == 1)
			std::cout << "PCF: Percentage Closer Filtering" << std::endl;
		else
			std::cout << "Poisson Sampling: Not Stratified or Rotated" << std::endl;

		shaderManager->getShader("Shadow Map")->setUniformi("filterMode", filterValue);
		glUseProgram(0);
	}

	// increase and decrease attenuation
	if ( keys[SDL_SCANCODE_4] ){
		for(int i = 0; i < numberOfScenePointLights; i++)
		{
			scenePointLights[i]->updateAttenuation(glm::vec3(1.0, scenePointLights[i]->getLight().attenuation[1]+0.0001, scenePointLights[i]->getLight().attenuation[2])); 
		}
	}

	if ( keys[SDL_SCANCODE_5] ){
		for(int i = 0; i < numberOfScenePointLights; i++)
		{
			scenePointLights[i]->updateAttenuation(glm::vec3(1.0, scenePointLights[i]->getLight().attenuation[1]-0.0001, scenePointLights[i]->getLight().attenuation[2])); 
		}
	}
	
	if ( keys[SDL_SCANCODE_6] ){
		for(int i = 0; i < numberOfScenePointLights; i++)
		{
			scenePointLights[i]->updateAttenuation(glm::vec3(1.0, scenePointLights[i]->getLight().attenuation[1], scenePointLights[i]->getLight().attenuation[2]-0.00001)); 
		}
	}

	if ( keys[SDL_SCANCODE_7] ){
		for(int i = 0; i < numberOfScenePointLights; i++)
		{
			scenePointLights[i]->updateAttenuation(glm::vec3(1.0, scenePointLights[i]->getLight().attenuation[1], scenePointLights[i]->getLight().attenuation[2]+0.00001)); 
		}
	}

	// change the current md2 animation
	if ( keys[SDL_SCANCODE_Z] ){md2Model->incrementAnimation();}

	if ( keys[SDL_SCANCODE_X] ){md2Model->decreaseAnimation();}

	if ( keys[SDL_SCANCODE_KP_9] && omniLightActive == true && lightRotation == false){scenePointLights[0]->moveUp(0.1f);}
	if ( keys[SDL_SCANCODE_KP_3] && omniLightActive == true && lightRotation == false){scenePointLights[0]->moveUp(-0.1f);}
	if ( keys[SDL_SCANCODE_KP_8] && omniLightActive == true && lightRotation == false){scenePointLights[0]->moveRight(0.1f);}
	if ( keys[SDL_SCANCODE_KP_2] && omniLightActive == true && lightRotation == false){scenePointLights[0]->moveRight(-0.1f);}
	if ( keys[SDL_SCANCODE_KP_6] && omniLightActive == true && lightRotation == false){scenePointLights[0]->moveForward(0.1f);}
	if ( keys[SDL_SCANCODE_KP_4] && omniLightActive == true && lightRotation == false){scenePointLights[0]->moveForward(-0.1f);}

	// switch rotating lights on so shadow point light can be moved around 
	if ( keys[SDL_SCANCODE_Q] && delayCounter > 30 )
	{
		if(lightRotation == true)
			lightRotation = false;
		else
			lightRotation = true;

		delayCounter = 0;
	}

	// function that turns off the lights one by one as I is pressed, does it with a simple loop
	// and break, it breaks the loop once an active light has been found and turned off. Otherwise it loops through
	// them all as normal. 
	if ( keys[SDL_SCANCODE_I] && delayCounter > 30)
	{
		bool loopBreak = false;
		delayCounter = 0;
		for(int i = 0; i < numberOfScenePointLights; i++)
		{
			if(scenePointLights[i]->getLight().lightActive == true)
			{
				scenePointLights[i]->updateLightActive(false);
				loopBreak = true;
			}
			
			if(loopBreak == true)
				break;
		}

		// outputs the number of lights currently active
		int lightCount = 0;
		for(int i = 0; i < numberOfScenePointLights; i++)
		{
			 lightCount += scenePointLights[i]->getLight().lightActive; 
		}
		 std::cout << "Active Lights: " << lightCount << std::endl;
	}

	// same as above except that it activates lights
	if ( keys[SDL_SCANCODE_O] && delayCounter > 30)
	{
		bool loopBreak = false;
		delayCounter = 0;
		for(int i = 0; i < numberOfScenePointLights; i++)
		{
			if(scenePointLights[i]->getLight().lightActive == false)
			{
				scenePointLights[i]->updateLightActive(true);
				loopBreak = true;
			}
			
			if(loopBreak == true)
				break;
		}

		// outputs the number of lights currently active
		int lightCount = 0;
		for(int i = 0; i < numberOfScenePointLights; i++)
		{
			 lightCount += scenePointLights[i]->getLight().lightActive; 
		}

		 std::cout << "Active Lights: " << lightCount << std::endl;
	}


	if(lightRotation == true)
	{
		// rotate the point lights in a circle and update there current position
		for(int i = 0; i < numberOfScenePointLights; i++)
		{
			angle[i] += 0.1;
			scenePointLights[i]->updatePosition(glm::vec4(-10 + (cos(angle[i] * 0.017453293) * 18), scenePointLights[i]->getPosition().y, -10 + (sin(angle[i] *0.017453293) * 18), 1));
		}
	}

	// send the updated light positions and attenuation to the Combined Shader 
	shaderManager->getShader("Combined Shader")->setActiveShader();
	
	// sending array of light structures to the shader, one per light
	lightStruct tempLightArray[8];
	for(int i = 0; i < numberOfScenePointLights; i++)
	{
		tempLightArray[i] = scenePointLights[i]->getLight();
	}

	shaderManager->getShader("Combined Shader")->setLights(tempLightArray, numberOfScenePointLights, lightArrayNames);

	// update light position for a single light, using uniform vec4fv instead of resending the whole
	// structure
	shaderManager->getShader("Normal Map")->setActiveShader();

	// update light position, the normal map uses the sceneShadowMapLights ambient, diffuse and specular but switches the position of the light
	// based on which lighting style is active
	if(omniLightActive == true)
	{
		shaderManager->getShader("Normal Map")->setUniformVec4fv("light.position", glm::value_ptr(scenePointLights[0]->getPosition()));
	}
	else
	{
		shaderManager->getShader("Normal Map")->setUniformVec4fv("light.position", glm::value_ptr(sceneShadowMapLight->getPosition()));
	}

	// set active shader as simple cube pass and send the point light position that we wish to render shadows from to the shader 
	shaderManager->getShader("SimpleCubePass")->setActiveShader();
	shaderManager->getShader("SimpleCubePass")->setUniformVec4fv("lightPosition", glm::value_ptr(scenePointLights[0]->getPosition()));

	// various view matrices/look at matrices for rendering the point light/omni-light shadows
	// light view matrix pointing down the pos x 
	lightViewMatrices[0] = glm::lookAt(glm::vec3(scenePointLights[0]->getPosition().x, scenePointLights[0]->getPosition().y, scenePointLights[0]->getPosition().z), glm::vec3(scenePointLights[0]->getPosition().x+1, scenePointLights[0]->getPosition().y, scenePointLights[0]->getPosition().z), glm::vec3(0, -1, 0));

	// light view matrix pointing down the neg x
	lightViewMatrices[1] = glm::lookAt(glm::vec3(scenePointLights[0]->getPosition().x, scenePointLights[0]->getPosition().y, scenePointLights[0]->getPosition().z), glm::vec3(scenePointLights[0]->getPosition().x-1, scenePointLights[0]->getPosition().y, scenePointLights[0]->getPosition().z), glm::vec3(0, -1, 0));

	// light view matrix pointing down the pos y
	lightViewMatrices[2] = glm::lookAt(glm::vec3(scenePointLights[0]->getPosition().x, scenePointLights[0]->getPosition().y, scenePointLights[0]->getPosition().z), glm::vec3(scenePointLights[0]->getPosition().x, scenePointLights[0]->getPosition().y+1, scenePointLights[0]->getPosition().z), glm::vec3(0, 0, 1));

	// light view matrix pointing down the neg y
	lightViewMatrices[3] = glm::lookAt(glm::vec3(scenePointLights[0]->getPosition().x, scenePointLights[0]->getPosition().y, scenePointLights[0]->getPosition().z), glm::vec3(scenePointLights[0]->getPosition().x, scenePointLights[0]->getPosition().y-1, scenePointLights[0]->getPosition().z), glm::vec3(0, 0, -1));

	// light view matrix pointing down the pos z
	lightViewMatrices[4] = glm::lookAt(glm::vec3(scenePointLights[0]->getPosition().x, scenePointLights[0]->getPosition().y, scenePointLights[0]->getPosition().z), glm::vec3(scenePointLights[0]->getPosition().x, scenePointLights[0]->getPosition().y, scenePointLights[0]->getPosition().z+1), glm::vec3(0, -1, 0));
		
	// light view matrix pointing down the neg z 
	lightViewMatrices[5] = glm::lookAt(glm::vec3(scenePointLights[0]->getPosition().x, scenePointLights[0]->getPosition().y, scenePointLights[0]->getPosition().z), glm::vec3(scenePointLights[0]->getPosition().x, scenePointLights[0]->getPosition().y, scenePointLights[0]->getPosition().z-1), glm::vec3(0, -1, 0));

	// animate md2 model
	md2Model->animateMesh();

	// rotate the cube continually
	objMeshList[4]->updateRotation(glm::vec3(objMeshList[4]->getRotation().x+0.1, objMeshList[4]->getRotation().y+0.1, objMeshList[4]->getRotation().z+0.1));

}

// deleting the various objects in the scene
cDemoState::~cDemoState()
{
	delete shaderManager;
	delete camera;
	delete modelAndTextureHandler;
	delete testCMap;
	delete md2Model;
	delete sceneMaterial;
	delete sceneShadowMapLight;

	for(int i = 0; i < numberOfScenePointLights; i++)
		delete scenePointLights[i];
	
	for(int i = 0; i < numberOfObjMesh; i++)
	{
		delete objMeshList[i];
	}
	
}

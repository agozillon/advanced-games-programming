#include "ObjModelHolder.h"

// constructor that takes in an indexcount, mesh ID and texture ID and sets them
ObjModelHolder::ObjModelHolder(GLuint meshID, GLuint indexCount, GLuint textureID)
{
	texture = textureID;
	meshIndexCount = indexCount;
	meshObject = meshID; 
}

// taking in a shader/material/matrix setting the material, binding the texture and setting the uniform and then drawing the obj model
void ObjModelHolder::drawModel(GLuint shaderProgramID, rt3d::materialStruct materialInUse, glm::mat4 passedInMVMatrix)
{
	glBindTexture(GL_TEXTURE_2D, texture);
	rt3d::setUniformMatrix4fv(shaderProgramID, "modelview", glm::value_ptr(passedInMVMatrix));
	rt3d::setMaterial(shaderProgramID, materialInUse);
	rt3d::drawMesh(meshObject, meshIndexCount, GL_TRIANGLES);
}

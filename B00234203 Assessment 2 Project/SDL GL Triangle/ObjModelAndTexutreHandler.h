#ifndef OBJMODELANDTEXTUREHANDLER_H
#define OBJMODELANDTEXTUREHANDLER_H
#include "rt3dObjLoader.h"				// including the rt3dObjLoader so that I can load in Obj Models 
#include "rt3d.h"						// including the rt3d library for its functionallity
#include <map>                          // including the Map class so I can use maps
#include <string>                       // including string for referencing the maps
#include <GL/glew.h>					// including glew so I can use GLuints
#include <vector>

// USED IN RT3D modified to use Maps instead of vectors, and calculate tangents on loading the model in

// class used to load in and store Obj models and textures so that they only need to be loaded once
class ObjModelAndTextureHandler
{

public:
	ObjModelAndTextureHandler(){}                                                         // blank inline constructor
	~ObjModelAndTextureHandler(){}														  // blank inline destructor
	void loadAndStoreObjModel(char *modelFileName, bool createTangents);				  // function that takes a model name and loads in the model and then stores it within the correct map, can also create tangents
	void loadAndStoreTexture(char *textureFileName);									  // function that takes a texture name and a boolean and loads in the texture then stores it within the correct map
	void getObjModel(GLuint &meshID, GLuint &meshIndexCount, char *modelFileName);		  // function that takes in two GLuints as a reference and the file name of the wanted obj model in the map and then changes the passed in GLuints and sets them to the wanted obj model ID/vectorCount
	int getNumberOfObjModelsStored(){return objMeshList.size();}					      // function that returns the current number of obj models stored as an integer
	void getTextureID(GLuint &textureID, char *textureFileName);						  // function that takes in a GLuint as a reference and the file name of the wanted texture in the map and then changes the passed in GLuint and sets it to the wanted texture ID 
	int getNumberOfTexturesStored(){return textureIDList.size();}						  // function that returns the number of textures stored as an integer

private:
	// maps of GLuints to hold the loaded in objects MeshID and IndexCounts as well as the texturesID
	
	std::map<std::string, GLuint> indexCountList;
	std::map<std::string, GLuint> objMeshList;
	std::map<std::string, GLuint> textureIDList;

};

#endif
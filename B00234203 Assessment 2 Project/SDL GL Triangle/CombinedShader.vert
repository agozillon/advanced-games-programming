// phong-tex.vert
// Vertex shader for use with a Phong or other reflection model fragment shader
// Calculates and passes on V, L, N vectors for use in fragment shader, phong2.frag
#version 400

#define MAXNUMBEROFLIGHTS 8

struct lightStruct
{
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
	vec4 position;
	vec3 attenuation;
	bool lightActive;
};

uniform mat4 modelview;
uniform mat4 projection;
uniform mat4 model;
uniform mat4 viewMatrix;
uniform lightStruct lights[MAXNUMBEROFLIGHTS];

//uniform mat3 normalmatrix;

in  vec3 in_Position;
in  vec3 in_Normal;
in  vec4 in_Tangent;

out vec3 ex_N;
out vec3 ex_V;
out vec3 ex_ModelSpaceV;


in vec2 in_TexCoord;
out vec2 ex_TexCoord;
out vec4 wPosition;

out mat3 TBN;
out mat4 invModel;


// multiply each vertex position by the MVP matrix
// and find V, L, N vectors for the fragment shader
void main(void) {
	// vertex into eye coordinates
	vec4 vertexPosition = modelview * vec4(in_Position,1.0);

	// passing unnormalized and unchanged to -ve eye co-ord vert position
	// will do that there alongside all the other per-fragment light calculations
	ex_V = vertexPosition.xyz;

	// surface normal in eye coordinates
	// taking the rotation part of the modelview matrix to generate the normal matrix
	// (if scaling is includes, should use transpose inverse modelview matrix!)
	// this is somewhat wasteful in compute time and should really be part of the cpu program,
	// giving an additional uniform input
	mat3 normalmatrix = transpose(inverse(mat3(modelview)));
	ex_N = normalize(normalmatrix * in_Normal);
	
	// calculating inverse model matrix to transfer anything in world space back
	// to model space.
	invModel = inverse(model);

	// calculating bitangent, cross product of tangent and normal, the W scales it -ve or positive
	// depending on the handidness of the tangent/vertices
	vec3 bitangent = (cross(in_Tangent.xyz, in_Normal) * in_Tangent.w);
	vec3 wNorm = in_Normal;
	vec3 tangent = in_Tangent.xyz;

	// creating TBN matrix that can transfer from Object/Model Space to Tangent Space
	TBN = mat3(tangent.xyz, bitangent, wNorm);

	// transfer the model space vertex into tangent space for future use
	ex_ModelSpaceV = in_Position; 
	
	// passing world space position for later conversion and comparing with shadows
	wPosition = model * vec4(in_Position, 1.0);
	
	// passing texture co-ord
	ex_TexCoord = in_TexCoord;

	// clip space co-ordinate
    gl_Position = projection * vertexPosition;

}
#ifndef SHADERMANAGER_H
#define SHADERMANAGER_H
#include <map>
#include <string>
#include <GL\glew.h>
#include "Shader.h"

// shader manager creates and contains a map of shaders that we can access by name
class ShaderManager{

public:
	ShaderManager(){} // blank inline constructor
	~ShaderManager(); // destructor that destroys all the shader* in the manager
	void createShaderProgram(const char* programName, const char* vertFile, const char *fragFile); // takes a map key name, vert file and frag file name and creates a shader
	Shader * getShader(const char* programName);  // passes a pointer from the named shader back as a return value 
	void addAttribute(GLuint index, char* attribName); // add an attribute to the attribute list that will get used at shader creation
	void addDefaultAttributes();  // creates the default attribute list texCoords, positions, normals.
	void clearAttributes();     // clears the current attribute list

private:
	// maps for holding shaders, attriubutes  
	std::map<std::string, Shader*> shaderList; 
	std::map<GLuint, char*> attribList;
	
	void printShaderError(const GLint shader); // prints shader errors, private function
	char* loadFile(const char *fname, GLint &fSize); // loads in a file, in this case shader files
	std::string loadFile(char *fileName); // same as the above just my version
	GLuint initShaders(const char *vertFile, const char *fragFile); // combines the frag and vert shaders and combines them into a program then passes the ID back
};
#endif
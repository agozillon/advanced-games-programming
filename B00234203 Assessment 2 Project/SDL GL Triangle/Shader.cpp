#include "Shader.h"
#include <glm/gtc/type_ptr.hpp>
#include <iostream>
#include <string>

void Shader::setUniformMatrix4fv(const char* uniformName, const GLfloat *data) {
	int uniformIndex = glGetUniformLocation(shaderID, uniformName);
	glUniformMatrix4fv(uniformIndex, 1, GL_FALSE, data); 
}

void Shader::setUniformVec3fv(const char* uniformName, float x, float y, float z) {
	int uniformIndex = glGetUniformLocation(shaderID, uniformName);
	glUniform3f(uniformIndex, x, y, z); 
}

// sets a uniform float by getting the index using the uniform name
void Shader::setUniformFloat(const char * uniformName, const GLfloat data)
{
	GLuint uniformIndex;
	uniformIndex = glGetUniformLocation(shaderID, uniformName);
	glUniform1f(uniformIndex, data);
}

// sets a light structure named light in the shader, won't work
// for any light structure named something else in the shader
void Shader::setLight(const lightStruct light) {
	// pass in light data to shader
	int uniformIndex = glGetUniformLocation(shaderID, "light.ambient");
	glUniform4fv(uniformIndex, 1, light.ambient);
	uniformIndex = glGetUniformLocation(shaderID, "light.diffuse");
	glUniform4fv(uniformIndex, 1, light.diffuse);
	uniformIndex = glGetUniformLocation(shaderID, "light.specular");
	glUniform4fv(uniformIndex, 1, light.specular);
	uniformIndex = glGetUniformLocation(shaderID, "light.position");
	glUniform4fv(uniformIndex, 1, light.position);
	uniformIndex = glGetUniformLocation(shaderID, "light.attenuation");
	glUniform3fv(uniformIndex, 1, light.attenuation);
	uniformIndex = glGetUniformLocation(shaderID, "light.lightActive");
	glUniform1i(uniformIndex, light.lightActive);
}

// sets a material structure named material in the shader, won't work
// for any material structure named something else in the shader
void Shader::setMaterial(const materialStruct material) {
	// pass in material data to shader 
	int uniformIndex = glGetUniformLocation(shaderID, "material.ambient");
	glUniform4fv(uniformIndex, 1, material.ambient);
	uniformIndex = glGetUniformLocation(shaderID, "material.diffuse");
	glUniform4fv(uniformIndex, 1, material.diffuse);
	uniformIndex = glGetUniformLocation(shaderID, "material.specular");
	glUniform4fv(uniformIndex, 1, material.specular);
	uniformIndex = glGetUniformLocation(shaderID, "material.shininess");
	glUniform1f(uniformIndex, material.shininess);
}

// finds the index using the unfirom name and then passes the data
void Shader::setUniformVec4fv(char * uniformName, const GLfloat *data) 
{
	GLuint uniformIndex = glGetUniformLocation(shaderID, uniformName);
	glUniform4fv(uniformIndex, 1, data);

}

// takes in a lightstructure array name I.E lights[0], lights[1] and the number of the
// lights in the array and then loops through them all adding .position at the end
// and then finding the uniform and passing it to the shader!
void Shader::setMultipleLightPos(glm::vec4 lightPos[],  int numberOf, char * uniformNames[])
{
	std::string current;

	for(int i = 0; i < numberOf; i++)
	{
		current = uniformNames[i];
		current += ".position";

		int uniformIndex = glGetUniformLocation(shaderID, current.c_str());
		glUniform4fv(uniformIndex, 1, glm::value_ptr(lightPos[i]));
	}

}

// works the same as the lightPos except it sends full light structures!
void Shader::setLights(lightStruct light[], int numberOf, char * uniformNames[])
{
	int uniformIndex;
	std::string current;

	for(int i = 0; i < numberOf; i++)
	{	
		current = uniformNames[i];
		current += ".ambient";
		// pass in light data to shader
		uniformIndex = glGetUniformLocation(shaderID, current.c_str());
		glUniform4fv(uniformIndex, 1, light[i].ambient);

		current = uniformNames[i];
		current += ".diffuse";

		uniformIndex = glGetUniformLocation(shaderID, current.c_str());
		glUniform4fv(uniformIndex, 1, light[i].diffuse);
		
		current = uniformNames[i];
		current += ".specular";

		uniformIndex = glGetUniformLocation(shaderID, current.c_str());
		glUniform4fv(uniformIndex, 1, light[i].specular);
		
		current = uniformNames[i];
		current += ".position";

		uniformIndex = glGetUniformLocation(shaderID, current.c_str());
		glUniform4fv(uniformIndex, 1, light[i].position);
		
		current = uniformNames[i];
		current += ".attenuation";

		uniformIndex = glGetUniformLocation(shaderID, current.c_str());
		glUniform3fv(uniformIndex, 1, light[i].attenuation);
		
		current = uniformNames[i];
		current += ".lightActive";
		uniformIndex = glGetUniformLocation(shaderID, current.c_str());
		glUniform1i(uniformIndex, light[i].lightActive);
	}
	
}

// get a uniform name and send an integer to the shader
void Shader::setUniformi(char * uniformName, const GLuint assignedIndex)
{
	GLuint uniformIndex;
	uniformIndex = glGetUniformLocation(shaderID, uniformName);
	glUniform1i(uniformIndex, assignedIndex);
}

void Shader::setActiveShader()
{
	glUseProgram(shaderID);
}
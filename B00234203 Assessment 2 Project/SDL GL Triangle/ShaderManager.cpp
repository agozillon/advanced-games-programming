#include "ShaderManager.h"
#include <iostream>
#include <fstream>
using namespace std;

// creates a shader program and adds it onto the map!
void ShaderManager::createShaderProgram(const char* programName, const char* vertFile, const char* fragFile)
{
		Shader * temp;
		temp = new Shader();
		shaderList[programName] = temp;
		shaderList[programName]->setShaderID(initShaders(vertFile, fragFile));
}

// returns the shader specified by the passed in char string
Shader * ShaderManager::getShader(const char* programName)
{
	return shaderList.find(programName)->second;
}

// loadFile - loads text file from file fname as a char* 
// Allocates memory - so remember to delete after use
// size of file returned in fSize
char* ShaderManager::loadFile(const char *fname, GLint &fSize) {

	int size;
	char * memblock;

	// file read based on example in cplusplus.com tutorial
	// ios::ate opens file at the end
	ifstream file (fname, ios::in|ios::binary|ios::ate);
	if (file.is_open()) {
		size = (int) file.tellg(); // get location of file pointer i.e. file size
		fSize = (GLint) size;
		memblock = new char [size];
		file.seekg (0, ios::beg);
		file.read (memblock, size);
		file.close();
		cout << "file " << fname << " loaded" << endl;
	}
	else {
		cout << "Unable to open file " << fname << endl;
		fSize = 0;
		// should ideally set a flag or use exception handling
		// so that calling function can decide what to do now
		return nullptr;
	}
	return memblock;
}

std::string ShaderManager::loadFile(char *fileName)
{
	std::fstream fs;
	std::string tempS;

	fs.open(fileName);

	if (fs.is_open())
	{
		while(!fs.eof())
		{
			char c = fs.get();

			// stops appending if we get any file issues stops garbage characters
			// being appended.
			if(fs.good())
			tempS += c;
		}

		fs.close();
	}
	else
		std::cout << "file could not be opened";

//	std::cout << tempS << std::endl;

	return tempS;
}

// printShaderError
// Display (hopefully) useful error messages if shader fails to compile or link
void ShaderManager::printShaderError(const GLint shader) {
	int maxLength = 0;
	int logLength = 0;
	GLchar *logMessage;

	// Find out how long the error message is
	if (!glIsShader(shader))
		glGetProgramiv(shader, GL_INFO_LOG_LENGTH, &maxLength);
	else
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &maxLength);

	if (maxLength > 0) { // If message has some contents
		logMessage = new GLchar[maxLength];
		if (!glIsShader(shader))
			glGetProgramInfoLog(shader, maxLength, &logLength, logMessage);
		else
			glGetShaderInfoLog(shader,maxLength, &logLength, logMessage);
		cout << "Shader Info Log:" << endl << logMessage << endl;
		delete [] logMessage;
	}
	// should additionally check for OpenGL errors here
}

// initilizes the shader
GLuint ShaderManager::initShaders(const char *vertFile, const char *fragFile) {
	GLuint p, f, v;

	char *vs,*fs;
	
	v = glCreateShader(GL_VERTEX_SHADER);
	f = glCreateShader(GL_FRAGMENT_SHADER);	
	
	// load shaders & get length of each
	GLint vlen;
	GLint flen;
	vs = loadFile(vertFile,vlen);
	fs = loadFile(fragFile,flen);
	
	const char * vv = vs;
	const char * ff = fs;

	glShaderSource(v, 1, &vv,&vlen);
	glShaderSource(f, 1, &ff,&flen);
	
	GLint compiled;

	glCompileShader(v);
	glGetShaderiv(v, GL_COMPILE_STATUS, &compiled);
	if (!compiled) {
		cout << "Vertex shader not compiled." << endl;
		printShaderError(v);
	} 

	glCompileShader(f);
	glGetShaderiv(f, GL_COMPILE_STATUS, &compiled);
	if (!compiled) {
		cout << "Fragment shader not compiled." << endl;
		printShaderError(f);
	} 
	
	p = glCreateProgram();
		
	glAttachShader(p,v);
	glAttachShader(p,f);

	// loop through current attriblist and use for this shader link
	for(std::map<GLuint, char*>::iterator it = attribList.begin(); it!=attribList.end(); ++it)
	{
		glBindAttribLocation(p, it->first, it->second);
	}

	glLinkProgram(p);
	glUseProgram(p);

	delete [] vs; // dont forget to free allocated memory
	delete [] fs; // we allocated this in the loadFile function...
	
	return p;
}

// adds an attribute onto the map
void ShaderManager::addAttribute(GLuint index, char* attribName)
{
	attribList[index] = attribName;
}

// clears the current attribute map
void ShaderManager::clearAttributes()
{
	attribList.clear();
}

// sets up the default attributes for the map that
// most shaders use
void ShaderManager::addDefaultAttributes()
{
	attribList[0] = "in_Position";
	attribList[1] = "in_Color";
	attribList[2] = "in_Normal";
	attribList[3] = "in_TexCoord";
}

// deletes all the shader objects currently instantiated
ShaderManager::~ShaderManager()
{
	for(std::map<std::string, Shader*>::iterator it = shaderList.begin(); it!=shaderList.end(); ++it)
	{
		delete it->second;
	}

	shaderList.clear();
}
#include "ObjMesh.h"
#include <stack>

// a constructor that takes in mesh related data and sets it to the origin, with no rotation
// and normal scale
ObjMesh::ObjMesh(const GLuint mesh, const GLuint vertCount, const GLuint texID, Shader * shader)
{
	activeShader = shader;
	meshID = mesh;
	shadowCast = false;
	textureID = texID;
	position = glm::vec3(0,0,0);
	rotation = glm::vec3(0,0,0);
	scalar = glm::vec3(1,1,1);
	meshVertexCount = vertCount;
}

// constructor that takes in mesh related data and sets it to a specified translation, rotation and scalar
ObjMesh::ObjMesh(const glm::vec3 pos, const glm::vec3 rot, const glm::vec3 scale, const bool castsShadow, const GLuint mesh, const GLuint vertCount, const GLuint texID, Shader * shader)
{
	activeShader = shader;
	meshID = mesh;
	shadowCast = castsShadow;
	position = pos;
	rotation = rot;
	scalar = scale;
	textureID = texID;
	meshVertexCount = vertCount;
}

void ObjMesh::draw(glm::mat4 viewMatrix)
{
	std::stack<glm::mat4> tmp;
	
	glActiveTexture(GL_TEXTURE0);

	// if it has a valid textureID, bind it
	if(textureID > 0)
	{
		glBindTexture(GL_TEXTURE_2D, textureID);
	}

	// rotate, translate and scale it
	tmp.push(glm::mat4(1.0));
	tmp.top() = glm::translate(tmp.top(), position);
	tmp.top() = glm::rotate(tmp.top(), rotation.x, glm::vec3(1.0f, 0.0f, 0.0f));
	tmp.top() = glm::rotate(tmp.top(), rotation.y, glm::vec3(0.0f, 1.0f, 0.0f));
	tmp.top() = glm::rotate(tmp.top(), rotation.z, glm::vec3(0.0f, 0.0f, 1.0f));
	tmp.top() = glm::scale(tmp.top(), scalar);

	// model matrix needed for casting omni shadows, so if the shader implements
	// them and the cast is set to true then send the model matrix otherwise
	// prepare for some undefined OpenGL behaviour yay!
	if(shadowCast == true)
	activeShader->setUniformMatrix4fv("model", glm::value_ptr(tmp.top()));
	
	// setting up and passing the modelview matrix to the shader
	tmp.top() = viewMatrix * tmp.top(); 
	activeShader->setUniformMatrix4fv("modelview", glm::value_ptr(tmp.top()));
	rt3d::drawIndexedMesh(meshID, meshVertexCount, GL_TRIANGLES);
	tmp.pop();
}
#include "MD2Mesh.h"
#include <stack>

// a constructor sets it to origin with no translations or rotation
MD2Mesh::MD2Mesh(const char * md2FileName, const GLuint texID, Shader * shader)
{
	// loading in md2Model and setting the currentAnimation to it's original 
	model = new md2model();
	meshID = model->ReadMD2Model(md2FileName);
	meshVertexCount = model->getVertDataCount();
	currentAnim = 0;

	activeShader = shader;
	shadowCast = false;
	textureID = texID;
	position = glm::vec3(0,0,0);
	rotation = glm::vec3(0,0,0);
	scalar = glm::vec3(1,1,1);
}

// a constructor sets it to a specified rotation, translation and scalar
MD2Mesh::MD2Mesh(const char * md2FileName, const glm::vec3 pos, const glm::vec3 rot, const glm::vec3 scale, const bool castsShadow, const GLuint texID, Shader * shader)
{
	// loading in md2Model and setting the currentAnimation to it's original 
	model = new md2model();
	meshID = model->ReadMD2Model(md2FileName);
	meshVertexCount = model->getVertDataCount();
	currentAnim = 0;
	
	activeShader = shader;
	shadowCast = castsShadow;
	position = pos;
	rotation = rot;
	scalar = scale;
	textureID = texID;
}

void MD2Mesh::incrementAnimation()
{
	if (++currentAnim >= 20) currentAnim = 0;
	std::cout << "Current animation: " << currentAnim << std::endl;
}

void MD2Mesh::decreaseAnimation()
{
	if (--currentAnim < 0) currentAnim = 19;
	std::cout << "Current animation: " << currentAnim << std::endl;
}

void MD2Mesh::animateMesh()
{
	// Animate the md2 model, and update the mesh with new vertex data
	model->Animate(currentAnim,0.1);
	rt3d::updateMesh(meshID, RT3D_VERTEX, model->getAnimVerts(), model->getVertDataSize());
}

void MD2Mesh::draw(glm::mat4 viewMatrix)
{
	std::stack<glm::mat4> tmp;
	
	// if it has a valid textureID, bind it
	if(textureID > 0)
	{
		glBindTexture(GL_TEXTURE_2D, textureID);
	}

	// rotate, translate and scale it
	tmp.push(glm::mat4(1.0));
	tmp.top() = glm::translate(tmp.top(), position);
	tmp.top() = glm::rotate(tmp.top(), rotation.x, glm::vec3(1.0f, 0.0f, 0.0f));
	tmp.top() = glm::rotate(tmp.top(), rotation.y, glm::vec3(0.0f, 1.0f, 0.0f));
	tmp.top() = glm::rotate(tmp.top(), rotation.z, glm::vec3(0.0f, 0.0f, 1.0f));
	tmp.top() = glm::scale(tmp.top(), scalar);

	// model matrix needed for casting omni shadows, so if the shader implements
	// them and the cast is set to true then send the model matrix otherwise
	// prepare for some undefined OpenGL behaviour yay!
	if(shadowCast == true)
	activeShader->setUniformMatrix4fv("model", glm::value_ptr(tmp.top()));
	
	// sending the standard modelview matrix
	tmp.top() = viewMatrix * tmp.top(); 
	activeShader->setUniformMatrix4fv("modelview", glm::value_ptr(tmp.top()));
	rt3d::drawMesh(meshID, meshVertexCount, GL_TRIANGLES);

	tmp.pop();
}
#include "Light.h"
#include <math.h>

Light::Light(vec4 ambient, vec4 diffuse, vec4 specular, vec4 position, vec3 attenuation, bool lightActive)
{
	light.ambient[0] = ambient.x; light.ambient[1] = ambient.y; light.ambient[2] = ambient.z; light.ambient[3] = ambient.w;
	light.diffuse[0] = diffuse.x; light.diffuse[1] = diffuse.y; light.diffuse[2] = diffuse.z; light.diffuse[3] = diffuse.w;
	light.specular[0] = specular.x; light.specular[1] = specular.y; light.specular[2] = specular.z; light.specular[3] = specular.w;
	light.position[0] = position.x; light.position[1] = position.y; light.position[2] = position.z; light.position[3] = position.w;
	light.attenuation[0] = attenuation.x; light.attenuation[1] = attenuation.y; light.attenuation[2] = attenuation.z;
	light.lightActive = lightActive; 
}

void Light::updateLightActive(bool active)
{
	light.lightActive = active;
}

void Light::updateAmbient(vec4 ambient)
{
	light.ambient[0] = ambient.x; light.ambient[1] = ambient.y; light.ambient[2] = ambient.z; light.ambient[3] = ambient.w;
}

void Light::updateDiffuse(vec4 diffuse)
{
	light.diffuse[0] = diffuse.x; light.diffuse[1] = diffuse.y; light.diffuse[2] = diffuse.z; light.diffuse[3] = diffuse.w;
}

void Light::updateSpecular(vec4 specular)
{
	light.specular[0] = specular.x; light.specular[1] = specular.y; light.specular[2] = specular.z; light.specular[3] = specular.w;
}

void Light::updatePosition(vec4 position)
{
	light.position[0] = position.x; light.position[1] = position.y; light.position[2] = position.z; light.position[3] = position.w;
}

void Light::updateAttenuation(vec3 attenuation)
{
	light.attenuation[0] = attenuation.x; light.attenuation[1] = attenuation.y; light.attenuation[2] = attenuation.z;
}

// simple move functions for the light, don't really need anything like the camera 
// move functions as these are designed for moving the Point Light and not the 
// directional light
void Light::moveForward(float dist)
{
	light.position[2] = light.position[2] + dist;
}

void Light::moveRight(float dist)
{
	light.position[0] = light.position[0] + dist;
}

void Light::moveUp(float dist)
{
	light.position[1] = light.position[1] + dist;
}
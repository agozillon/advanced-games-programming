#include "rt3d.h"
#include <map>

using namespace std;
#define DEG_TO_RADIAN 0.017453293

namespace rt3d {

// struct vaoBuffers will be used inside the rt3d library
// clients should not need to know about this data structure
struct vaoBuffers {
	GLuint vertex_buffer;
	GLuint colour_buffer;
	GLuint normal_buffer;
	GLuint texcoord_buffer;
	GLuint index_buffer;
};

static map<GLuint, GLuint *> vertexArrayMap;

// Something went wrong - print error message and quit
void exitFatalError(const char *message) {
    cout << message << " ";
    exit(1);
}

// loadFile - loads text file from file fname as a char* 
// Allocates memory - so remember to delete after use
// size of file returned in fSize
char* loadFile(const char *fname, GLint &fSize) {
	int size;
	char * memblock;

	// file read based on example in cplusplus.com tutorial
	// ios::ate opens file at the end
	ifstream file (fname, ios::in|ios::binary|ios::ate);
	if (file.is_open()) {
		size = (int) file.tellg(); // get location of file pointer i.e. file size
		fSize = (GLint) size;
		memblock = new char [size];
		file.seekg (0, ios::beg);
		file.read (memblock, size);
		file.close();
		cout << "file " << fname << " loaded" << endl;
	}
	else {
		cout << "Unable to open file " << fname << endl;
		fSize = 0;
		// should ideally set a flag or use exception handling
		// so that calling function can decide what to do now
		return nullptr;
	}
	return memblock;
}


// simple error function ninja'd from OpenGL.org FAQ and modded slightly to output the error number
int checkGLErrors()
{
  int errCount = 0;
  for(GLenum currError = glGetError(); currError != GL_NO_ERROR; currError = glGetError())
  {
   //Do something with `currError`.
	 std::cout << "error number: " << currError << std::endl;
	

	 ++errCount;
  }
 
  return errCount;
}

// textToTexture
GLuint textToTexture(const char * str/* ,TTF_Font *font, SDL_Color colour, GLuint &w,GLuint &h */) {
	
	TTF_Font *font;

	// set up TrueType / SDL_ttf
	if (TTF_Init()== -1)
		cout << "TTF failed to initialise." << endl;

	font = TTF_OpenFont("MavenPro-Regular.ttf", 24);
	if (font == NULL)
		cout << "Failed to open font." << endl;

	
	SDL_Color colour = { 255, 255, 255 };
	SDL_Color bg = { 0, 0, 0 };

	SDL_Surface *stringImage;
	stringImage = TTF_RenderText_Blended(font,str,colour);

	if (stringImage == NULL)
		//exitFatalError("String surface not created.");
		std::cout << "String surface not created." << std::endl;

	GLuint w = stringImage->w;
	GLuint h = stringImage->h;
	GLuint colours = stringImage->format->BytesPerPixel;

	GLuint format, internalFormat;
	if (colours == 4) {   // alpha
		if (stringImage->format->Rmask == 0x000000ff)
			format = GL_RGBA;
	    else
		    format = GL_BGRA;
	} else {             // no alpha
		if (stringImage->format->Rmask == 0x000000ff)
			format = GL_RGB;
	    else
		    format = GL_BGR;
	}
	internalFormat = (colours == 4) ? GL_RGBA : GL_RGB;

	GLuint texture;

	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, w, h, 0,
                    format, GL_UNSIGNED_BYTE, stringImage->pixels);

	// SDL surface was used to generate the texture but is no longer
	// required. Release it to free memory
	SDL_FreeSurface(stringImage);
	
	TTF_CloseFont(font);
	
	return texture;
}

// A simple texture loading function
// lots of room for improvement - and better error checking!
GLuint loadBitmap(char *fname) {
	GLuint texID;
	glGenTextures(1, &texID); // generate texture ID

	// load file - using core SDL library
 	SDL_Surface *tmpSurface;
	tmpSurface = SDL_LoadBMP(fname);
	if (!tmpSurface) {
		std::cout << "Error loading bitmap" << std::endl;
	}

	// bind texture and set parameters
	glBindTexture(GL_TEXTURE_2D, texID);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	SDL_PixelFormat *format = tmpSurface->format;
	
	GLuint externalFormat, internalFormat;
	if (format->Amask) {
		internalFormat = GL_RGBA;
		externalFormat = (format->Rmask < format-> Bmask) ? GL_RGBA : GL_BGRA;
	}
	else {
		internalFormat = GL_RGB;
		externalFormat = (format->Rmask < format-> Bmask) ? GL_RGB : GL_BGR;
	}

	glTexImage2D(GL_TEXTURE_2D,0,internalFormat,tmpSurface->w, tmpSurface->h, 0,
		externalFormat, GL_UNSIGNED_BYTE, tmpSurface->pixels);
	glGenerateMipmap(GL_TEXTURE_2D);

	SDL_FreeSurface(tmpSurface); // texture loaded, free the temporary buffer
	return texID;	// return value of texture ID
}





// setting up the depth cube map and it's framebuffers, alongside all the various parameters
// it needs it's not very flexible at the moment which is fine!
GLuint setupDepthCubeMap(GLuint &dCube, GLuint &framebuffer, GLuint depthCube[])
{
	// standard cube map setup!
	glGenTextures(1, &dCube);
	glBindTexture(GL_TEXTURE_CUBE_MAP, dCube);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

	// normally GL_DEPTH_COMPONENT gives you the best size available unless stated otherwise
	// this time I ask specifically for the highest memory depth texture available no real reason
	// other than wanting to avoid floating point precision errors.
	for(int i = 0; i < 6; i++)
	{
		 glTexImage2D(depthCube[i], 0, GL_DEPTH_COMPONENT32F, 1024, 1024, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
	}

	// creating framebuffer looping through and attaching each side to the framebuffer
	glGenFramebuffers(1, &framebuffer);
	glBindFramebuffer(GL_FRAMEBUFFER, framebuffer);

	for(int i = 0; i < 6; i++)
	{
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, depthCube[i], dCube, 0);
	}

	// setting the DrawBufferMode/what i want sent to this buffer
	glDrawBuffer(GL_NONE);

	if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		std::cout << "framebuffer error" << std::endl;
	
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	
	return 0;
}

// function for generating a framebuffer and binding a texture to it, not the most flexible
// function at the moment(only accepting a single buffer) but it works and can be refactored as need arises
GLuint bindFramebufferTexture(GLuint textureID, GLenum textureAttach, GLenum drawBufferMode)
{
	GLuint tempFBO;

	// generate framebuffer, bind and then attach it to whatever texture is passed in and set the
	//type of Buffer Mode it is could be GL_DEPTH_ATTACHMENT, GL_DEPTH_STENCIL_ATTACHMENT, GL_COLOR_ATTACHMENTi etc
	glGenFramebuffers(1, &tempFBO);
	glBindFramebuffer(GL_FRAMEBUFFER, tempFBO);
	glFramebufferTexture2D(GL_FRAMEBUFFER, textureAttach, GL_TEXTURE_2D, textureID, 0);
	
	
	// setting the DrawBufferMode/what i want sent to this buffer
	glDrawBuffer(drawBufferMode);

	// unbinding current buffer, should go back to default
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	
	// simply checking if the framebuffer completed fine or if it has an error slightly modified
	// exert from opengl-tutorial.org 
	if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		std::cout << "framebuffer error" << std::endl;

	return tempFBO;
}

// had some help from Lengyel, Eric. �Computing Tangent Space Basis Vectors for an Arbitrary Mesh�. Terathon Software 3D Graphics Library, 2001. http://www.terathon.com/code/tangent.html
// for this function and opengl-tutorial.org/intermediate-tutorials/tutorial-13-normal-mapping
void createTangents(int vertexCount, vector<GLfloat> vertices,  vector<GLfloat> norms, vector<GLfloat> texCoords, vector<GLuint> indices, vector<GLfloat> &tangents)
{

	vector<glm::vec3> tan;
	vector<glm::vec3> tan1;
	vector<glm::vec4> tangent;

	tan.resize(vertexCount, glm::vec3(0, 0, 0));
	tan1.resize(vertexCount, glm::vec3(0, 0, 0));
	tangent.resize(vertexCount, glm::vec4(0,0,0,0));
	
	// exit early and output error message if a model has no texture UVs 
	if(texCoords.size() == 0)
	{
		std::cout << "model has no texture co-ordinates can't calculate tangents" << std::endl;
		return;
	}

	for(int i = 0; i < indices.size(); i+=3)
	{
		GLuint currentIndices[3];
		currentIndices[0] = indices.data()[i];
		currentIndices[1] = indices.data()[i+1];
		currentIndices[2] = indices.data()[i+2];

		glm::vec3 vertex[3];
		// getting vertices related to the indices pulled from the vector
		vertex[0].x = vertices[currentIndices[0] * 3];
		vertex[0].y = vertices[(currentIndices[0] * 3)+1];
		vertex[0].z = vertices[(currentIndices[0] * 3)+2];
		vertex[1].x = vertices[currentIndices[1] * 3];
		vertex[1].y = vertices[(currentIndices[1] * 3)+1];
		vertex[1].z = vertices[(currentIndices[1] * 3)+2];
		vertex[2].x = vertices[currentIndices[2] * 3];
		vertex[2].y = vertices[(currentIndices[2] * 3)+1];
		vertex[2].z = vertices[(currentIndices[2] * 3)+2];

		// same as above except with tex coords
		glm::vec2 tCoords[3];
		tCoords[0].x = texCoords[currentIndices[0] * 2];
		tCoords[0].y = texCoords[(currentIndices[0] * 2)+1];
		tCoords[1].x = texCoords[currentIndices[1] * 2];
		tCoords[1].y = texCoords[(currentIndices[1] * 2)+1];
	    tCoords[2].x = texCoords[currentIndices[2] * 2];
		tCoords[2].y = texCoords[(currentIndices[2] * 2)+1];
		
		// calculating the edge vectors and UVs on this triangle to transform, all 
		// use vertex[0] I visualize it as getting a corner like so |/
		// similar to caclulating normals
		glm::vec3 Q1; 
		glm::vec3 Q2;

		Q1 = vertex[1] - vertex[0]; // Q1 
		Q2 = vertex[2] - vertex[0]; // Q2  
		

		glm::vec2 uv1;
		glm::vec2 uv2;

		uv1 = tCoords[1] - tCoords[0]; 
		uv2 = tCoords[2] - tCoords[0]; 
	    
		// rate of change across U
		float coefficent = 1.0f / (uv1.x * uv2.y - uv1.y * uv2.x); 
		glm::vec3 sdir = (Q1 * uv2.y - Q2 * uv1.y) * coefficent; 
	
		glm::vec3 tdir = (Q2 * uv1.x - Q1 * uv2.x) * coefficent;
	
		tan.data()[currentIndices[0]] = sdir;
		tan.data()[currentIndices[1]] = sdir;
		tan.data()[currentIndices[2]] = sdir;

		tan1.data()[currentIndices[0]] = tdir;
		tan1.data()[currentIndices[1]] = tdir;
		tan1.data()[currentIndices[2]] = tdir;
	
	}

	// putting normals into vec3 vector so they're easier to deal with
	vector<glm::vec3> normals;
	for(int i = 0; i < vertexCount; i+=3)
	{
		normals.push_back(glm::vec3(norms.data()[i], norms.data()[i+1], norms.data()[i+2]));
	}

	for(int i = 0; i < vertexCount / 3; i++)
	{
		glm::vec3 n = normals.data()[i];
		glm::vec3 t = tan.data()[i];
		
		// calculate which order the vertices are in/handidness
		// cross product of normal and tangent projected against bitangent if less than 0 then
		// "oh wait! that's facing the wrong way" so we make the scale value -1.0 if it's > 1.0
		// then it's fine so we don't need to negate. 
		tangent[i].w = (glm::dot(glm::cross(n, t), tan1.data()[i]) < 0.0f) ? -1.0f : 1.0f;

		// Gram-Schmidt orthogonalize, orthogonalizes the tangent to the normal and allows
		// us to use a transpose instead of an inverse within the shader speeding calculations up
		// projectecting t onto n get's the difference between the two and then multiplying that
		// by the normal gets the value that we wish to negate the tangent by to orthogonolize it
		tangent[i] = glm::vec4(glm::normalize(t - n * glm::dot(n, t)), tangent[i].w);
		
		tangents.push_back(tangent[i].x);
		tangents.push_back(tangent[i].y);
		tangents.push_back(tangent[i].z);
		tangents.push_back(tangent[i].w);

	}

	
}

// function that creates a blank texture (maybe not the best idea to call it bufferTexture? meh) usually used for the framebuffer
// reasonably flexible could do with a lot more work. But essentially allows you to setup a texture with or without a depth render buffer
// and to pass in a texture type (RGB etc) and an active texture value. this parts maybe not required but 99% safetys always nice.
GLuint createBufferTexture(GLuint textureW, GLuint textureH, bool hasDepthBuffer, GLuint &depthBufferID, GLenum textureType, GLenum activeTexture)
{
	// generate and active the texture
	GLuint tempTex;
	glGenTextures(1, &tempTex);
	glActiveTexture(activeTexture);

	// setting up texture variables such as size of buffer, type of values and internal format/original
	// format which in the case of my shadow buffer SHOULD be Depth buffer values hence GL_DEPTH_COMPONENT which
	// specifies each value is a depth value and converts values to float within the 0 to 1 range
	// also means only a single value is stored in the texture instead of RGBA etc. 
	// and then we pass NULL instead of data for a blank texture. 
	glBindTexture(GL_TEXTURE_2D, tempTex);
	glTexImage2D(GL_TEXTURE_2D, 0, textureType,  textureW, textureH, 0,
		textureType, GL_FLOAT, NULL);
	
	// sets up a depth buffer for the texture, allows the framebuffer to render depth values to it
	// admitedly not looked hugely into it but it's here for future use at the moment
	if(hasDepthBuffer == true)
	{
		// The depth buffer, not required for shadow map buffer
		glGenRenderbuffers(1, &depthBufferID);
		glBindRenderbuffer(GL_RENDERBUFFER, depthBufferID);
		glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, textureW, textureH);
		glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthBufferID);
	}

	// setting minification and magnification filters for the texture
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	// setting texture compare values for this texture, one if not the only defining feature
	// of a shadow sampler texture. Essentially allows your texture looks up too compare(instead of normally passing
	// back colour values) the Z value of the lookup with the Z value of the texture using the defined
	// TEXTURE_COMPARE_FUNC (in this case GL_LESS) and then pass back a 0 if it fails and a 1 if it passes
	// with linear filtering and shadow sampler a textureProj look up will linearly sample 4 surrounding texels (up one, down one, left one, right one)
	// and for each texel compare it to the depth and if it passes add 0.25 to the total if not it's 0 and has failed
	// up to a total of 1.0 if all of them pass and 0 if they all fail. alongside anywhere in between.
	// essentially in-built opengl PCF Filtering 
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_COMPARE_MODE, GL_COMPARE_REF_TO_TEXTURE);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_COMPARE_FUNC, GL_LESS);
	
	// values outside of the frustum too the edge values so all values remain within the 0 to 1 spectrum
	// side effect being all values outside of the depth texture = in shadow, feel free to try this over the border
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	
	// Cookbook Exert, essentially the lesser of two evils, clamp to edge makes everything
	// outside of the lights view clamp to 0 essentially making everything outside the view dark
	// this makes everything outside the view 1.0 or the edge of the texture which is what they
	// get compared to. Which makes them all un-shadowed the downside being that if a shadow goes out of 
	// the lights view it disappears. This only works until objects leave the light projection's far plane or 
	// get so close they intersect its near plane.
	GLfloat border[] ={1.0f, 0.0f, 0.0f, 0.0f};
	glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, border);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
	
	// deactivate texture
	glActiveTexture(0);
	glBindTexture(GL_TEXTURE_2D, 0);
	
	//checkGLErrors();
	return tempTex;
}


GLuint createMesh(const GLuint numVerts, const GLfloat* vertices, const GLfloat* colours, 
	const GLfloat* normals, const GLfloat* texcoords, const GLuint indexCount, const GLuint* indices) {
	GLuint VAO;
	// generate and set up a VAO for the mesh
	glGenVertexArrays(1, &VAO);
	glBindVertexArray(VAO);

	GLuint *pMeshBuffers = new GLuint[5];


	if (vertices == nullptr) {
		// cant create a mesh without vertices... oops
		exitFatalError("Attempt to create a mesh with no vertices");
	}

	// generate and set up the VBOs for the data
	GLuint VBO;
	glGenBuffers(1, &VBO);
	
	// VBO for vertex data
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, 3*numVerts*sizeof(GLfloat), vertices, GL_STATIC_DRAW);
	glVertexAttribPointer((GLuint)RT3D_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, 0); 
	glEnableVertexAttribArray(RT3D_VERTEX);
	pMeshBuffers[RT3D_VERTEX] = VBO;


	// VBO for colour data
	if (colours != nullptr) {
		glGenBuffers(1, &VBO);
		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glBufferData(GL_ARRAY_BUFFER, 3*numVerts*sizeof(GLfloat), colours, GL_STATIC_DRAW);
		glVertexAttribPointer((GLuint)RT3D_COLOUR, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(RT3D_COLOUR);
		pMeshBuffers[RT3D_COLOUR] = VBO;
	}

	// VBO for normal data
	if (normals != nullptr) {
		glGenBuffers(1, &VBO);
		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glBufferData(GL_ARRAY_BUFFER, 3*numVerts*sizeof(GLfloat), normals, GL_STATIC_DRAW);
		glVertexAttribPointer((GLuint)RT3D_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(RT3D_NORMAL);
		pMeshBuffers[RT3D_NORMAL] = VBO;
	}

	// VBO for tex-coord data
	if (texcoords != nullptr) {
		glGenBuffers(1, &VBO);
		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glBufferData(GL_ARRAY_BUFFER, 2*numVerts*sizeof(GLfloat), texcoords, GL_STATIC_DRAW);
		glVertexAttribPointer((GLuint)RT3D_TEXCOORD, 2, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(RT3D_TEXCOORD);
		pMeshBuffers[RT3D_TEXCOORD] = VBO;
	}

	if (indices != nullptr && indexCount > 0) {
		glGenBuffers(1, &VBO);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, VBO);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, indexCount * sizeof(GLuint), indices, GL_STATIC_DRAW);
		pMeshBuffers[RT3D_INDEX] = VBO;
	}
	// unbind vertex array
	glBindVertexArray(0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	// return the identifier needed to draw this mesh

	vertexArrayMap.insert( pair<GLuint, GLuint *>(VAO, pMeshBuffers) );

	return VAO;
}

GLuint createMesh(const GLuint numVerts, const GLfloat* vertices, const GLfloat* colours, 
	const GLfloat* normals, const GLfloat* texcoords) {
	return createMesh(numVerts, vertices, colours, normals, texcoords, 0, nullptr);
}

GLuint createMesh(const GLuint numVerts, const GLfloat* vertices) {
	return createMesh(numVerts, vertices, nullptr, nullptr, nullptr);
}

GLuint createColourMesh(const GLuint numVerts, const GLfloat* vertices, const GLfloat* colours) {
	return createMesh(numVerts, vertices, colours, nullptr, nullptr);
}

// set matrices untested... likely to change - not totally happy with this for now.
void setMatrices(const GLuint program, const GLfloat *proj, const GLfloat *mv, const GLfloat *mvp) {
	int uniformIndex = glGetUniformLocation(program, "modelview");
	glUniformMatrix4fv(uniformIndex, 1, GL_FALSE, mv);
	uniformIndex = glGetUniformLocation(program, "projection");
	glUniformMatrix4fv(uniformIndex, 1, GL_FALSE, proj);
	uniformIndex = glGetUniformLocation(program, "MVP");
	glUniformMatrix4fv(uniformIndex, 1, GL_FALSE, mvp);
	uniformIndex = glGetUniformLocation(program, "normalmatrix");
	GLfloat nm[9] = {	mvp[0], mvp[1], mvp[2],
						mvp[4], mvp[5], mvp[6],
						mvp[8], mvp[8], mvp[20] };	
	glUniformMatrix4fv(uniformIndex, 1, GL_FALSE, nm);
}

void setProjection(const GLuint program, const GLfloat *data) {
	int uniformIndex = glGetUniformLocation(program, "projection");
	glUniformMatrix4fv(uniformIndex, 1, GL_FALSE, data); 
}

void drawMesh(const GLuint mesh, const GLuint numVerts, const GLuint primitive) {
	glBindVertexArray(mesh);	// Bind mesh VAO
	glDrawArrays(primitive, 0, numVerts);	// draw first vertex array object
	glBindVertexArray(0);
}


void drawIndexedMesh(const GLuint mesh, const GLuint indexCount, const GLuint primitive) {
	glBindVertexArray(mesh);	// Bind mesh VAO
	glDrawElements(primitive, indexCount,  GL_UNSIGNED_INT, 0);	// draw VAO 
	glBindVertexArray(0);
}


void updateMesh(const GLuint mesh, const unsigned int bufferType, const GLfloat *data, const GLuint size) {
	GLuint * pMeshBuffers = vertexArrayMap[mesh];
	glBindVertexArray(mesh);

	// Delete the old buffer data
	glDeleteBuffers(1, &pMeshBuffers[bufferType]);

	// generate and set up the VBOs for the new data
	GLuint VBO;
	glGenBuffers(1, &VBO);
		// VBO for the data
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, size*sizeof(GLfloat), data, GL_STATIC_DRAW);
	glVertexAttribPointer((GLuint)bufferType, 3, GL_FLOAT, GL_FALSE, 0, 0); 
	glEnableVertexAttribArray(bufferType);
	pMeshBuffers[RT3D_VERTEX] = VBO;

	glBindVertexArray(0);

}

} // namespace rt3d